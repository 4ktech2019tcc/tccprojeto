﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Venda.Cliente
{
    class BusinessCliente
    {
        public void cadastrar (Database.Model.tb_cliente c)
        {

            if (c.nm_nome == string.Empty)
            {
                throw new ArgumentException("Informe o nome");
            }
            if (c.ds_email == string.Empty)
            {
                throw new ArgumentException("Informe o Email");
            }
            if (c.ds_CPF == string.Empty)
            {
                throw new ArgumentException("Informe o CPF");
            }
            if (c.ds_CEP == string.Empty)
            {
                throw new ArgumentException("Informe o CEP");
            }
            if (c.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Informe o Telefone");
            }
            if (c.ds_celular == string.Empty)
            {
                throw new ArgumentException("Informe o Telefone");
            }
            if (c.ds_estado == string.Empty)
            {
                throw new ArgumentException("Informe o estado");
            }
            Database.Vendas.Cliente.DatabaseCliente db = new Database.Vendas.Cliente.DatabaseCliente();
            db.cadastrar(c);


        }
        public void alterar(Database.Model.tb_cliente c)
        {

            if (c.nm_nome == string.Empty)
            {
                throw new ArgumentException("Informe o nome");
            }
            if (c.ds_email == string.Empty)
            {
                throw new ArgumentException("Informe o Email");
            }
            if (c.ds_CPF == string.Empty)
            {
                throw new ArgumentException("Informe o CPF");
            }
            if (c.ds_CEP == string.Empty)
            {
                throw new ArgumentException("Informe o CEP");
            }
            if (c.ds_telefone == string.Empty)
            {
                throw new ArgumentException("Informe o Telefone");
            }
            if (c.ds_celular == string.Empty)
            {
                throw new ArgumentException("Informe o Telefone");
            }
            if (c.ds_estado == string.Empty)
            {
                throw new ArgumentException("Informe o estado");
            }

            Database.Vendas.Cliente.DatabaseCliente db = new Database.Vendas.Cliente.DatabaseCliente();
            db.alterar(c);


        }



    }
}
