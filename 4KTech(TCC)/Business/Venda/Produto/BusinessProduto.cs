﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Venda.Produto
{
    class BusinessProduto
    {
        public void Salvar  (Database.Model.tb_produto_venda v)
        {
            if (v.nm_produto == string.Empty)
            {
                throw new ArgumentException("Nome do produto é obrigatório");
            }


            if (v.vl_preço == 0)
            {
                throw new ArgumentException("Preço do produto é obrigatório");
            }

            if (v.nm_produto == string.Empty)
            {
                throw new ArgumentException("Categoria do produto é obrigatório");
            }

            Database.Vendas.Produto.DatabaseProduto db = new Database.Vendas.Produto.DatabaseProduto();
            db.salvar(v);
        }

        public void alterar (Database.Model.tb_produto_venda v)
        {
            if (v.nm_produto == string.Empty)
            {
                throw new ArgumentException("Nome do produto é obrigatório");
            }


            if (v.vl_preço == 0)
            {
                throw new ArgumentException("Preço do produto é obrigatório");
            }

            if (v.nm_produto == string.Empty)
            {
                throw new ArgumentException("Categoria do produto é obrigatório");
            }

            Database.Vendas.Produto.DatabaseProduto db = new Database.Vendas.Produto.DatabaseProduto();
            db.alterar(v);
        }

    }

}
