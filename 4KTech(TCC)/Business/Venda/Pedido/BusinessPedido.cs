﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Venda.Pedido
{
    class BusinessPedido
    {
        public void Cadastrarvenda(Database.Model.tb_venda venda)
        {

            if (venda.dt_venda == null)
                throw new ArgumentException("Data é obrigatório");
            if (venda.fk_id_cliente == 0)
                throw new ArgumentException("Fornecedor é obrigatório");


            Database.Vendas.Pedido.DatabasePedido db = new Database.Vendas.Pedido.DatabasePedido();
            db.Cadastrarvanda(venda);
        }
        public void CadastrarVendaitem(Database.Model.tb_venda_item venda)
        {

            if (venda.fk_id_produto_venda == 0)
                throw new ArgumentException("id da compra nao encontrado");
            if (venda.fk_id_venda == 0)
                throw new ArgumentException("id do pruduto nao encontrado");


            Database.Vendas.Pedido.DatabasePedido db = new Database.Vendas.Pedido.DatabasePedido();
            db.Cadastrarvendaitem(venda);
        }
    }
}
