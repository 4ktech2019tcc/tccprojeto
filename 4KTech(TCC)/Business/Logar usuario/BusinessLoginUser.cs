﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Logar_usuario
{
    class BusinessLoginUser
    {
        public Database.Model.tb_funcionario Logar(string user, string senha)
        {
            if (user == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório.");
            }

            Database.Logar_usuario.DatabaseLogarUser db = new Database.Logar_usuario.DatabaseLogarUser();
            Database.Model.tb_funcionario validação = db.logar(user, senha);

            return validação;
        }

    }
}
