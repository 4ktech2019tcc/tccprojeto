﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.RH.FolhaDePagamento
{
    class BusinessFolhaDePagamento
    {
        public void cadastrar(Database.Model.tb_folha_pagamento c)
        {

            if (c.fk_id_funcionario == 0)
            {
                throw new ArgumentException("Informe o ID do Funcionario");
            }

            Database.RH.FolhaDePagamento.DatabaseFolhaDePagamento db = new Database.RH.FolhaDePagamento.DatabaseFolhaDePagamento();
            db.CadastrarFolha(c);


        }
    }
}
