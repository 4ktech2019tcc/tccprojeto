﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.funcionario
{
    class businessCadastroFuncionario
    {
        public void cadastrar(Database.Model.tb_funcionario f)
        {
            if (f.nm_funcionario == string.Empty)
            {
                throw new ArgumentException("Nome do funcionario é obrigatório");

            }

            if (f.ds_endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");

            }

            if (f.vl_salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório");

            }

            if (f.ds_usuario == string.Empty)
            {
                throw new ArgumentException("Nome do usuário é obrigatório");

            }

            if (f.ds_senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");

            }

            Database.funcionario.FuncionarioDatabase db = new Database.funcionario.FuncionarioDatabase();
            db.cadastrar(f);


        }
        public void Alterar(Database.Model.tb_funcionario f)
        {
            if (f.nm_funcionario == string.Empty)
            {
                throw new ArgumentException("Nome do funcionario é obrigatório");

            }

            if (f.ds_endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");

            }

            if (f.vl_salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório");

            }

            if (f.ds_usuario == string.Empty)
            {
                throw new ArgumentException("Nome do usuário é obrigatório");

            }

            if (f.ds_senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");

            }

            Database.funcionario.FuncionarioDatabase db = new Database.funcionario.FuncionarioDatabase();
            db.cadastrar(f);



        }
        public List<Database.Model.tb_funcionario> consultarPorNome (string nome)
        {
            if (nome == string.Empty)
            {
                throw new ArgumentException("informe o nome");
            }

            Database.funcionario.FuncionarioDatabase db = new Database.funcionario.FuncionarioDatabase();
            List<Database.Model.tb_funcionario> list = db.consultar(nome);


            return list;


        }
    }
}
