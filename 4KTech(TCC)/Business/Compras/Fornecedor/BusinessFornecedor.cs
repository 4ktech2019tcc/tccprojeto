﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Compras
{
    class BusinessFornecedor
    {
        public void cadastrarfornecedor (Database.Model.tb_fornecedor f)
        {
            if(f.ds_cidade == string.Empty)
                throw new ArgumentException("Cidade é obrigatório");
            if (f.ds_CNPJ == string.Empty)
                throw new ArgumentException("CNPJ é obrigatório");
            if (f.ds_CEP == string.Empty)
                throw new ArgumentException("CEP é obrigatório");
            if (f.ds_estado == string.Empty)
                throw new ArgumentException("Estado é obrigatório");
            if (f.ds_telefone == string.Empty)
                throw new ArgumentException("Telefone é obrigatório");
            if (f.nm_nome == string.Empty)
                throw new ArgumentException("Nome do fornecedor é obrigatório");

            Database.Compras.Fornedor.DatabaseFornecedor fornecedor = new Database.Compras.Fornedor.DatabaseFornecedor();
            fornecedor.CadastrarFornecedor(f);
        }
        public void Alterarfornecedor(Database.Model.tb_fornecedor f)
        {
            if (f.ds_cidade == string.Empty)
                throw new ArgumentException("Cidade é obrigatório");
            if (f.ds_CNPJ == string.Empty)
                throw new ArgumentException("CNPJ é obrigatório");
            if (f.ds_CEP == string.Empty)
                throw new ArgumentException("CEP é obrigatório");
            if (f.ds_estado == string.Empty)
                throw new ArgumentException("Estado é obrigatório");
            if (f.ds_telefone == string.Empty)
                throw new ArgumentException("Telefone é obrigatório");
            if (f.nm_nome == string.Empty)
                throw new ArgumentException("Nome do fornecedor é obrigatório");

            Database.Compras.Fornedor.DatabaseFornecedor fornecedor = new Database.Compras.Fornedor.DatabaseFornecedor();
            fornecedor.AlterarFornecedor(f);
        }

    }
}
