﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Compras.Produto
{
    class BusinessProduto
    {
        public void CadastrarProduto (Database.Model.tb_produto_compra A)
        {

            if (A.nm_produto == string.Empty)
                throw new ArgumentException("Nome do produto é obrigatório");
            if (A.nm_marca == string.Empty)
                throw new ArgumentException("Categoria é obrigatório");
            if (A.vl_preco == 0)
                throw new ArgumentException("Valor é obrigatório");
            if (A.vl_quantidade == 0)
                throw new ArgumentException("Quantidade é obrigatório");



            Database.Compras.Produto.DatabaseProduto B = new Database.Compras.Produto.DatabaseProduto();
            B.CadastrarProduto(A);
        }
        public void AlterarProduto(Database.Model.tb_produto_compra A)
        {

            if (A.nm_produto == string.Empty)
                throw new ArgumentException("Nome do produto é obrigatório");
            if (A.nm_marca == string.Empty)
                throw new ArgumentException("Categoria é obrigatório");
            if (A.vl_preco == 0)
                throw new ArgumentException("Valor é obrigatório");
            if (A.vl_quantidade == 0)
                throw new ArgumentException("Quantidade é obrigatório");


            Database.Compras.Produto.DatabaseProduto B = new Database.Compras.Produto.DatabaseProduto();
            B.AlterarProduto(A);
        }
    }
}
