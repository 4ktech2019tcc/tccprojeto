﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Business.Compras.Pedido
{
    class BusinessPedido
    {
        public void CadastrarProduto(Database.Model.tb_compra compra)
        {

            if (compra.dt_compra == null)
                throw new ArgumentException("Data é obrigatório");
            if (compra.fk_fornecedor == 0)
                throw new ArgumentException("Fornecedor é obrigatório");


            Database.Compras.Pedido.DatabasePedido db = new Database.Compras.Pedido.DatabasePedido();
            db.CadastrarPedido(compra);
        }
        public void AlterarProduto(Database.Model.tb_produto_compra A)
        {

            if (A.nm_produto == string.Empty)
                throw new ArgumentException("Nome do produto é obrigatório");
            if (A.nm_marca == string.Empty)
                throw new ArgumentException("Categoria é obrigatório");
            if (A.vl_preco == 0)
                throw new ArgumentException("Valor é obrigatório");
            if (A.vl_quantidade == 0)
                throw new ArgumentException("Quantidade é obrigatório");


            Database.Compras.Produto.DatabaseProduto B = new Database.Compras.Produto.DatabaseProduto();
            B.AlterarProduto(A);
        }
        public void CadastrarCompraitem(Database.Model.tb_compra_item compra)
        {

            if (compra.fk_id_compra == 0)
                throw new ArgumentException("id da compra nao encontrado");
            if (compra.fk_id_produto_compra == 0)
                throw new ArgumentException("id do pruduto nao encontrado");


            Database.Compras.Pedido.DatabasePedido db = new Database.Compras.Pedido.DatabasePedido();
            db.CadastrarPedidoitem(compra);
        }
    }
   
}
