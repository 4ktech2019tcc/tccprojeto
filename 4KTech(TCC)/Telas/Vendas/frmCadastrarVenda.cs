﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Vendas
{
    public partial class frmCadastrarVenda : UserControl
    {
        public frmCadastrarVenda()
        {
            InitializeComponent();
        }
        private void CarregarComboProduto()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_venda> list = db.tb_produto_venda.ToList();

            list.Insert(0, new Database.Model.tb_produto_venda { nm_produto = "" });

            // carrega as informações no combo 
            cboProduto.DisplayMember = nameof(Database.Model.tb_produto_venda.nm_produto);
            cboProduto.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }
        private void CarregarCombocliente()
        {
           
                Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_cliente> list = db.tb_cliente.ToList();

                list.Insert(0, new Database.Model.tb_cliente { nm_nome = "" });

                // carrega as informações no combo 
                cbocliente.DisplayMember = nameof(Database.Model.tb_cliente.nm_nome);
                cbocliente.DataSource = list;

                //cbolistfuncionario.Items.Insert(0, "Selecione");
            
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                int qtd = Convert.ToInt32(nudquantidade.Value);
                DateTime D = dtpDataDaCompra.Value;
                if (qtd == 0)
                {
                    MessageBox.Show("Insira a Quantidade", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }               
                else
                {
                    Database.Model.tb_produto_venda p = cboProduto.SelectedItem as Database.Model.tb_produto_venda;

                    List<Database.Model.tb_produto_venda> itens = dgvproduto.DataSource as List<Database.Model.tb_produto_venda>;

                    if (itens == null)
                        itens = new List<Database.Model.tb_produto_venda>();

                    for (int i = 0; i < qtd; i++)
                    {
                        itens.Add(p);
                    }

                    dgvproduto.DataSource = null;
                    dgvproduto.DataSource = itens;
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime D = dtpDataDaCompra.Value;
                if (D < DateTime.Now)
                {
                    MessageBox.Show("Insira uma data valida", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
                else
                {
                    Database.Model.tb_venda venda = new Database.Model.tb_venda();

                    Database.Model.tb_cliente idCliente = cbocliente.SelectedItem as Database.Model.tb_cliente;
                    int id = idCliente.id_cliente;

                    venda.fk_id_cliente = id;
                    venda.dt_venda = dtpDataDaCompra.Value.Date;

                    Business.Venda.Pedido.BusinessPedido busi = new Business.Venda.Pedido.BusinessPedido();
                    busi.Cadastrarvenda(venda);

                    Database.Vendas.Pedido.DatabasePedido C = new Database.Vendas.Pedido.DatabasePedido();
                    int idvenda = C.aultimoPidido(venda);

                    List<Database.Model.tb_produto_venda> itens = dgvproduto.DataSource as List<Database.Model.tb_produto_venda>;
                    foreach (Database.Model.tb_produto_venda item in itens)
                    {
                        Database.Model.tb_venda_item venda_Item = new Database.Model.tb_venda_item();

                        venda_Item.fk_id_produto_venda = item.id_produto_venda;
                        venda_Item.fk_id_venda = idvenda;

                        busi.CadastrarVendaitem(venda_Item);
                    }

                    MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    dgvproduto.DataSource = null;
                                       
                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }

        }





        private void cbocliente_Click_1(object sender, EventArgs e)
        {
            this.CarregarCombocliente();
        }


        private void cboProduto_Click_1(object sender, EventArgs e)
        {
            this.CarregarComboProduto();
        }

        private void nudquantidade_ValueChanged(object sender, EventArgs e)
        {

        }

        public void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Vendas.Produto.DatabaseProduto produto = new Database.Vendas.Produto.DatabaseProduto();
            List<Database.Model.tb_produto_venda> list = produto.consultar();

            Database.Model.tb_produto_venda P = cboProduto.SelectedItem as Database.Model.tb_produto_venda;

            decimal valor = Convert.ToInt32(P.vl_preço);
            lblUnidade.Text = Convert.ToString(valor);

            decimal quantidade = Convert.ToInt32(nudquantidade.Value);

            decimal ValorTotal = quantidade * valor;

            lblTotal.Text = Convert.ToString(ValorTotal);
        }

        private void nudquantidade_ValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                this.cboProduto_SelectedIndexChanged(sender, e);
            }
            catch (Exception)
            {
                MessageBox.Show("Insira um produto antes", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }
    }
}