﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Vendas
{
    public partial class frmAlterarClientee : UserControl
    {
        public frmAlterarClientee()
        {
            InitializeComponent();
        }

        private void CarregarCombo()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_cliente> list = db.tb_cliente.ToList();

            list.Insert(0, new Database.Model.tb_cliente { nm_nome = "" });

            // carrega as informações no combo 
            cbocliente.DisplayMember = nameof(Database.Model.tb_cliente.nm_nome);
            cbocliente.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_cliente cliente = cbocliente.SelectedItem as Database.Model.tb_cliente;
                int idCliente = cliente.id_cliente;

                string email = txtemail.Text.Trim();
                                 
                if (email.Contains("gmail"))
                {
                    Database.valid.ValidarEmail validar = new Database.valid.ValidarEmail();
                    int valida = validar.email(email);

                    if (valida == 1)
                    {
                                
                                
                                Database.Model.tb_cliente c = new Database.Model.tb_cliente();

                                c.id_cliente = idCliente;
                                c.ds_bairro = txtbairro.Text;
                                c.ds_celular = txtcelular.Text;
                                c.ds_CEP = txtcep.Text;
                                c.ds_cidade = txtcidade.Text;
                                c.ds_cliente = txtdescri.Text;
                                c.ds_CPF = txtcpf.Text;
                                c.ds_email = txtemail.Text;
                                c.ds_estado = cboestado.Text;
                                c.ds_rua = txtrua.Text;
                                c.ds_telefone = txttelefone.Text;
                                c.dt_data_cadastro = DateTime.Now;
                                c.nm_nome = txtnome.Text;

                                Business.Venda.Cliente.BusinessCliente busi = new Business.Venda.Cliente.BusinessCliente();
                                busi.alterar(c);

                                MessageBox.Show("Alterado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    }
                    else
                    {
                        MessageBox.Show("caracteres invalidos", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    MessageBox.Show("email invalido", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
           

            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

                                 
                               
        private void Cbocliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Model.tb_cliente cliente = cbocliente.SelectedItem as Database.Model.tb_cliente;
            string nome = cliente.nm_nome;

            Database.Vendas.Cliente.DatabaseCliente f = new Database.Vendas.Cliente.DatabaseCliente();
            List<Database.Model.tb_cliente> list = f.consultar(nome);

            txtbairro.Text = cliente.ds_bairro;
            txtcelular.Text = cliente.ds_celular;
            txtcep.Text = cliente.ds_CEP;
            txtcidade.Text = cliente.ds_cidade;
            txtdescri.Text = cliente.ds_cliente;
            txtcpf.Text = cliente.ds_CPF;
            txtemail.Text = cliente.ds_email;
            cboestado.Text = cliente.ds_estado;
            txtrua.Text = cliente.ds_rua;
            txttelefone.Text = cliente.ds_telefone;
            txtnome.Text = cliente.nm_nome;
        }

        private void Cbocliente_Click(object sender, EventArgs e)
        {
            this.CarregarCombo();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult resposta = MessageBox.Show("Deseja realmente Remover esse funcionario?", "4k tech",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    DialogResult resposta2 = MessageBox.Show("Esse Operação não pode ser desfeita, deseja continuar?", "4k tech",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                    if (resposta == DialogResult.Yes)
                    {
                        Database.Model.tb_cliente cliente = cbocliente.SelectedItem as Database.Model.tb_cliente;
                        int id = cliente.id_cliente;

                        Database.Vendas.Cliente.DatabaseCliente db = new Database.Vendas.Cliente.DatabaseCliente();
                        db.remover(id);

                        MessageBox.Show("Cliente removido", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }



        }
    }
}
