﻿namespace _4KTech_TCC_.Telas.Vendas
{
    partial class frmAlterarClientee
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbocliente = new System.Windows.Forms.ComboBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtrua = new System.Windows.Forms.TextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.cboestado = new System.Windows.Forms.ComboBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtdescri = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // cbocliente
            // 
            this.cbocliente.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.cbocliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocliente.Font = new System.Drawing.Font("Neon 80s", 8.25F);
            this.cbocliente.FormattingEnabled = true;
            this.cbocliente.Location = new System.Drawing.Point(181, 29);
            this.cbocliente.Name = "cbocliente";
            this.cbocliente.Size = new System.Drawing.Size(420, 19);
            this.cbocliente.TabIndex = 319;
            this.cbocliente.SelectedIndexChanged += new System.EventHandler(this.Cbocliente_SelectedIndexChanged);
            this.cbocliente.Click += new System.EventHandler(this.Cbocliente_Click);
            // 
            // txtbairro
            // 
            this.txtbairro.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtbairro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbairro.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtbairro.Location = new System.Drawing.Point(181, 282);
            this.txtbairro.MaxLength = 100;
            this.txtbairro.Multiline = true;
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(412, 27);
            this.txtbairro.TabIndex = 314;
            // 
            // txtrua
            // 
            this.txtrua.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtrua.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtrua.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtrua.Location = new System.Drawing.Point(181, 246);
            this.txtrua.MaxLength = 100;
            this.txtrua.Multiline = true;
            this.txtrua.Name = "txtrua";
            this.txtrua.Size = new System.Drawing.Size(412, 27);
            this.txtrua.TabIndex = 315;
            // 
            // txtcep
            // 
            this.txtcep.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcep.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtcep.Location = new System.Drawing.Point(181, 218);
            this.txtcep.Mask = "00000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(88, 19);
            this.txtcep.TabIndex = 313;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(125, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 317;
            this.label2.Text = "Bairro";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(131, 221);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 16);
            this.label11.TabIndex = 318;
            this.label11.Text = "CEP:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(133, 247);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 16);
            this.label12.TabIndex = 316;
            this.label12.Text = "Rua:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(142, 197);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 16);
            this.label14.TabIndex = 312;
            this.label14.Text = "Cel:";
            // 
            // txtcelular
            // 
            this.txtcelular.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtcelular.Location = new System.Drawing.Point(181, 190);
            this.txtcelular.Mask = "(00)00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(155, 19);
            this.txtcelular.TabIndex = 311;
            // 
            // cboestado
            // 
            this.cboestado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboestado.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.cboestado.FormattingEnabled = true;
            this.cboestado.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cboestado.Location = new System.Drawing.Point(181, 354);
            this.cboestado.Name = "cboestado";
            this.cboestado.Size = new System.Drawing.Size(181, 24);
            this.cboestado.TabIndex = 310;
            // 
            // txtcidade
            // 
            this.txtcidade.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcidade.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtcidade.Location = new System.Drawing.Point(181, 318);
            this.txtcidade.MaxLength = 100;
            this.txtcidade.Multiline = true;
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(181, 27);
            this.txtcidade.TabIndex = 309;
            // 
            // txtnome
            // 
            this.txtnome.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtnome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtnome.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtnome.Location = new System.Drawing.Point(181, 62);
            this.txtnome.MaxLength = 100;
            this.txtnome.Multiline = true;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(420, 27);
            this.txtnome.TabIndex = 308;
            // 
            // txtemail
            // 
            this.txtemail.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtemail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtemail.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtemail.Location = new System.Drawing.Point(181, 98);
            this.txtemail.MaxLength = 100;
            this.txtemail.Multiline = true;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(420, 27);
            this.txtemail.TabIndex = 307;
            // 
            // txttelefone
            // 
            this.txttelefone.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txttelefone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txttelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txttelefone.Location = new System.Drawing.Point(181, 162);
            this.txttelefone.Mask = "(00)0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(155, 19);
            this.txttelefone.TabIndex = 306;
            // 
            // txtdescri
            // 
            this.txtdescri.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtdescri.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtdescri.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtdescri.Location = new System.Drawing.Point(182, 387);
            this.txtdescri.MaxLength = 250;
            this.txtdescri.Multiline = true;
            this.txtdescri.Name = "txtdescri";
            this.txtdescri.Size = new System.Drawing.Size(412, 94);
            this.txtdescri.TabIndex = 305;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(93, 389);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 16);
            this.label13.TabIndex = 304;
            this.label13.Text = "Descrição:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(116, 354);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 16);
            this.label15.TabIndex = 303;
            this.label15.Text = "Estado:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(115, 318);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 16);
            this.label16.TabIndex = 302;
            this.label16.Text = "Cidade:";
            // 
            // txtcpf
            // 
            this.txtcpf.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcpf.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcpf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtcpf.Location = new System.Drawing.Point(181, 134);
            this.txtcpf.Mask = "000,000,000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(155, 19);
            this.txtcpf.TabIndex = 301;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(144, 163);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 16);
            this.label17.TabIndex = 300;
            this.label17.Text = "Tel:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(136, 135);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 16);
            this.label18.TabIndex = 299;
            this.label18.Text = "CPF:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(30, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 16);
            this.label1.TabIndex = 298;
            this.label1.Text = "Selecione o Cliente";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(120, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 16);
            this.label19.TabIndex = 298;
            this.label19.Text = "Nome:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(119, 93);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 16);
            this.label20.TabIndex = 297;
            this.label20.Text = "E-mail:";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(182, 493);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 56);
            this.button2.TabIndex = 296;
            this.button2.Text = "Deletar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(349, 493);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(244, 56);
            this.button1.TabIndex = 296;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::_4KTech_TCC_.Properties.Resources.vetores_exclusivos_de_atendimento_ao_cliente_7139_125;
            this.pictureBox1.Location = new System.Drawing.Point(622, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(292, 273);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 322;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Neon 80s", 14F);
            this.textBox1.Location = new System.Drawing.Point(622, 347);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(292, 202);
            this.textBox1.TabIndex = 320;
            this.textBox1.Text = "Aqui e onde são alteradas as informações do cliente \r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Neon 80s", 16F);
            this.label3.Location = new System.Drawing.Point(622, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 22);
            this.label3.TabIndex = 321;
            this.label3.Text = "Cliente";
            // 
            // frmAlterarClientee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cbocliente);
            this.Controls.Add(this.txtbairro);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtrua);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtcep);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtcelular);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.cboestado);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtcidade);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtdescri);
            this.Controls.Add(this.txttelefone);
            this.Name = "frmAlterarClientee";
            this.Size = new System.Drawing.Size(976, 609);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbocliente;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtrua;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.ComboBox cboestado;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.TextBox txtdescri;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
    }
}
