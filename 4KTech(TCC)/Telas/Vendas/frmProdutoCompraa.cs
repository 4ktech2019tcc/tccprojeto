﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Vendas
{
    public partial class frmProdutoCompraa : UserControl
    {
        public frmProdutoCompraa()
        {
            InitializeComponent();
        }


        private void CarregarComboProduto()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_venda> list = db.tb_produto_venda.ToList();

            list.Insert(0, new Database.Model.tb_produto_venda { nm_produto = "" });

            // carrega as informações no combo 
            cboProduto2.DisplayMember = nameof(Database.Model.tb_produto_venda.nm_produto);
            cboProduto2.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_produto_venda A = new Database.Model.tb_produto_venda();
                A.vl_quantidade = 1;
                A.nm_produto = txtNome1.Text;
                A.vl_preço = nudPreço1.Value;


                Business.Venda.Produto.BusinessProduto busi = new Business.Venda.Produto.BusinessProduto();
                busi.Salvar(A);

                MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtProdutos.Text.Trim();

                if (nome == string.Empty)
                {
                    MessageBox.Show("informe o nome", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Database.Vendas.Produto.DatabaseProduto db = new Database.Vendas.Produto.DatabaseProduto();
                List<Database.Model.tb_produto_venda> list = db.consultarNome(nome);
                dgvProdutos.DataSource = list;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Vendas.Produto.DatabaseProduto db = new Database.Vendas.Produto.DatabaseProduto();
                List<Database.Model.tb_produto_venda> list = db.consultar();
                dgvProdutos.DataSource = list;


            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Vendas.Produto.DatabaseProduto produto = new Database.Vendas.Produto.DatabaseProduto();
            List<Database.Model.tb_produto_venda> list = produto.consultar();

            Database.Model.tb_produto_venda p = cboProduto2.SelectedItem as Database.Model.tb_produto_venda;
            int id = p.id_produto_venda;
           
            txtnome2.Text= p.nm_produto;
            nudpreco2.Value = Convert.ToDecimal(p.vl_preço);

           
        }

        private void Button3_Click(object sender, EventArgs e)

        {
            Database.Model.tb_produto_venda p = cboProduto2.SelectedItem as Database.Model.tb_produto_venda;
            int id = p.id_produto_venda;

            Database.Model.tb_produto_venda A = new Database.Model.tb_produto_venda();
            A.vl_quantidade = 1;
            A.id_produto_venda = id;
            A.nm_produto = txtnome2.Text;
            A.vl_preço = nudpreco2.Value;

            Business.Venda.Produto.BusinessProduto busi = new Business.Venda.Produto.BusinessProduto();
            busi.alterar(A);
            MessageBox.Show("Alterado com sucesso", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void cboProduto2_Click(object sender, EventArgs e)
        {
            this.CarregarComboProduto();
        }
    }
}
