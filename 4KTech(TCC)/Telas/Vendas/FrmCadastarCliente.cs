﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace _4KTech_TCC_.Telas.Vendas
{
    public partial class FrmCadastarCliente : UserControl
    {
        public FrmCadastarCliente()
        {
            InitializeComponent();
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }
        public void SendMensagensDeTexto()
        {


            try
            {

                if (txtcelular.Text == string.Empty)
                {

                    MessageBox.Show("Enforme o Celular Do Cliente no Cadastro", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                if (txtmensagem.Text == string.Empty)
                {

                    MessageBox.Show("Enforme o Celular Do Cliente no Cadastro", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                string tel = txtcelular.Text.Trim();
                string men = txtmensagem.Text.Trim();



                Diferenciais.SMS.SendWhatss send = new Diferenciais.SMS.SendWhatss();
                send.enviarWhatsapp(tel, men);

                Diferenciais.SMS.SMSAPI send2 = new Diferenciais.SMS.SMSAPI();
                send2.enviarSMS(tel, men);


                MessageBox.Show("mensagem enviada", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Erro ao enviar a mensagem", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        public void MensagemDeCadastro()
        {
            string tel = txtcelular.Text.Trim();
            string men = txtmensagem.Text.Trim();

            string nome = txtnome.Text;

            men = "Olá" + " " +nome + " " + "Você foi Cadastrado(a) no nosso sistema, Aguarde por Promoções e muito mais.";
                                              
            Diferenciais.SMS.SMSAPI send2 = new Diferenciais.SMS.SMSAPI();
            send2.enviarSMS(tel, men);
                                    
        }
        public void loadCEp()
        {
            try
            {
                string cep = txtcep.Text;

                Diferenciais.Correio.CorreioApi correiorua = new Diferenciais.Correio.CorreioApi();
                string rua = correiorua.BuscarRUa(cep);

                Diferenciais.Correio.CorreioApi correiobairro = new Diferenciais.Correio.CorreioApi();
                string bairro = correiobairro.BuscarBairro(cep);

                // eh cidade 
                Diferenciais.Correio.CorreioApi correioestado = new Diferenciais.Correio.CorreioApi();
                string estado = correioestado.BuscarEstado(cep);

                txtrua.Text = rua;

                txtcidade.Text = estado;
                txtbairro.Text = bairro;
            }
            catch (Exception)
            {

                MessageBox.Show("complete os campos");
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                // pega o email do cliente
                string email = txtemail.Text.Trim();


                Database.valid.ValidarEmail email1 = new Database.valid.ValidarEmail();
                int b = email1.email(email);

                if (email.Contains("gmail"))
                {
                    if (b == 1)
                    {

                        Database.Model.tb_cliente c = new Database.Model.tb_cliente();
                        DateTime data = DateTime.Now;
                        c.nm_nome = txtnome.Text;
                        c.ds_bairro = txtbairro.Text;
                        c.ds_celular = txtcelular.Text;
                        c.ds_CEP = txtcep.Text;
                        c.ds_cidade = txtcidade.Text;
                        c.ds_cliente = txtdescri.Text;
                        c.ds_CPF = txtcpf.Text;
                        c.ds_email = txtemail.Text;
                        c.ds_estado = cboestado.Text;
                        c.ds_rua = txtrua.Text;
                        c.ds_telefone = txttelefone.Text;
                        c.dt_data_cadastro = data;

                        Business.Venda.Cliente.BusinessCliente busi = new Business.Venda.Cliente.BusinessCliente();
                        busi.cadastrar(c);

                        // envia um email para o cliente
                        Diferenciais.E_mail.EmailAPI enviar = new Diferenciais.E_mail.EmailAPI();
                        enviar.Enviar(email);


                        //Envia a Mensagem  de texto
                        this.MensagemDeCadastro();

                        MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        txtnome.Text = null;
                        txtbairro.Text = null;
                        txtcelular.Text = null;
                        txtcep.Text = null;
                        txtcidade.Text = null;
                        txtdescri.Text = null;
                        txtcpf.Text = null;
                        txtemail.Text = null;
                        cboestado.Text = null;
                        txtrua.Text = null;
                        txttelefone.Text = null;
                    }
                    else
                    {
                        MessageBox.Show("email invalido", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    }
                }
                else
                {
                    MessageBox.Show("Enforme um Gmail", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }

            
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
          
            
        }

        private void Txtcep_Leave(object sender, EventArgs e)
        {
            this.loadCEp();
        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void RdoSim_CheckedChanged(object sender, EventArgs e)
        {
            txtmensagem.Visible = true;
            btnEnviar.Visible = true;
            
        }

        private void RdoNão_CheckedChanged(object sender, EventArgs e)
        {
            btnEnviar.Visible = false;
            txtmensagem.Visible = false;
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            //send men;
            
            this.SendMensagensDeTexto();
        }

        private void FrmCadastarCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
