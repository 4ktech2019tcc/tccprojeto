﻿namespace _4KTech_TCC_.Telas.Vendas
{
    partial class FrmCadastarCliente
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtrua = new System.Windows.Forms.TextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.cboestado = new System.Windows.Forms.ComboBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtdescri = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtmensagem = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rdoNão = new System.Windows.Forms.RadioButton();
            this.rdoSim = new System.Windows.Forms.RadioButton();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtbairro);
            this.groupBox1.Controls.Add(this.txtrua);
            this.groupBox1.Controls.Add(this.txtcep);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtcelular);
            this.groupBox1.Controls.Add(this.cboestado);
            this.groupBox1.Controls.Add(this.txtcidade);
            this.groupBox1.Controls.Add(this.txtnome);
            this.groupBox1.Controls.Add(this.txtemail);
            this.groupBox1.Controls.Add(this.txttelefone);
            this.groupBox1.Controls.Add(this.txtdescri);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtcpf);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(58, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(534, 555);
            this.groupBox1.TabIndex = 162;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cadastrar cliente";
            // 
            // txtbairro
            // 
            this.txtbairro.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtbairro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbairro.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtbairro.Location = new System.Drawing.Point(102, 268);
            this.txtbairro.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtbairro.MaxLength = 100;
            this.txtbairro.Multiline = true;
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(413, 27);
            this.txtbairro.TabIndex = 291;
            // 
            // txtrua
            // 
            this.txtrua.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtrua.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtrua.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtrua.Location = new System.Drawing.Point(102, 235);
            this.txtrua.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtrua.MaxLength = 100;
            this.txtrua.Multiline = true;
            this.txtrua.Name = "txtrua";
            this.txtrua.Size = new System.Drawing.Size(412, 27);
            this.txtrua.TabIndex = 292;
            // 
            // txtcep
            // 
            this.txtcep.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcep.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcep.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtcep.Location = new System.Drawing.Point(102, 203);
            this.txtcep.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcep.Mask = "00000-000";
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(155, 19);
            this.txtcep.TabIndex = 290;
            this.txtcep.Leave += new System.EventHandler(this.Txtcep_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(42, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 294;
            this.label2.Text = "Bairro";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(48, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 16);
            this.label11.TabIndex = 295;
            this.label11.Text = "CEP:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(50, 238);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 16);
            this.label12.TabIndex = 293;
            this.label12.Text = "Rua:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(57, 173);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 20);
            this.label14.TabIndex = 289;
            this.label14.Text = "Cel:";
            // 
            // txtcelular
            // 
            this.txtcelular.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcelular.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtcelular.Location = new System.Drawing.Point(102, 173);
            this.txtcelular.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcelular.Mask = "+55 (00) 00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(155, 19);
            this.txtcelular.TabIndex = 288;
            // 
            // cboestado
            // 
            this.cboestado.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.cboestado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboestado.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.cboestado.ForeColor = System.Drawing.Color.Black;
            this.cboestado.FormattingEnabled = true;
            this.cboestado.Items.AddRange(new object[] {
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"});
            this.cboestado.Location = new System.Drawing.Point(102, 334);
            this.cboestado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cboestado.Name = "cboestado";
            this.cboestado.Size = new System.Drawing.Size(181, 24);
            this.cboestado.TabIndex = 287;
            // 
            // txtcidade
            // 
            this.txtcidade.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcidade.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcidade.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtcidade.Location = new System.Drawing.Point(102, 301);
            this.txtcidade.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcidade.MaxLength = 100;
            this.txtcidade.Multiline = true;
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(413, 27);
            this.txtcidade.TabIndex = 286;
            // 
            // txtnome
            // 
            this.txtnome.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtnome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtnome.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtnome.Location = new System.Drawing.Point(102, 38);
            this.txtnome.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtnome.MaxLength = 50;
            this.txtnome.Multiline = true;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(420, 27);
            this.txtnome.TabIndex = 285;
            // 
            // txtemail
            // 
            this.txtemail.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtemail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtemail.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtemail.Location = new System.Drawing.Point(102, 71);
            this.txtemail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtemail.MaxLength = 50;
            this.txtemail.Multiline = true;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(420, 27);
            this.txtemail.TabIndex = 284;
            this.txtemail.TextChanged += new System.EventHandler(this.txtemail_TextChanged);
            // 
            // txttelefone
            // 
            this.txttelefone.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txttelefone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txttelefone.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txttelefone.Location = new System.Drawing.Point(102, 139);
            this.txttelefone.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txttelefone.Mask = "(00)0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(155, 19);
            this.txttelefone.TabIndex = 283;
            // 
            // txtdescri
            // 
            this.txtdescri.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtdescri.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtdescri.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtdescri.Location = new System.Drawing.Point(103, 369);
            this.txtdescri.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtdescri.MaxLength = 50;
            this.txtdescri.Multiline = true;
            this.txtdescri.Name = "txtdescri";
            this.txtdescri.Size = new System.Drawing.Size(412, 95);
            this.txtdescri.TabIndex = 282;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(9, 370);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 16);
            this.label13.TabIndex = 281;
            this.label13.Text = "Descrição:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(29, 338);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 16);
            this.label15.TabIndex = 280;
            this.label15.Text = "Estado:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(30, 304);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 16);
            this.label16.TabIndex = 279;
            this.label16.Text = "Cidade:";
            // 
            // txtcpf
            // 
            this.txtcpf.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcpf.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcpf.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtcpf.Location = new System.Drawing.Point(102, 108);
            this.txtcpf.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtcpf.Mask = "000,000,000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(155, 19);
            this.txtcpf.TabIndex = 278;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(59, 139);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 20);
            this.label17.TabIndex = 277;
            this.label17.Text = "Tel:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(49, 111);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 16);
            this.label18.TabIndex = 276;
            this.label18.Text = "CPF:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(38, 41);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 16);
            this.label19.TabIndex = 275;
            this.label19.Text = "Nome:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(36, 69);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 16);
            this.label20.TabIndex = 274;
            this.label20.Text = "E-mail:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(103, 469);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(411, 71);
            this.button1.TabIndex = 165;
            this.button1.Text = "Cadastrar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtmensagem);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.rdoNão);
            this.groupBox2.Controls.Add(this.rdoSim);
            this.groupBox2.Controls.Add(this.btnEnviar);
            this.groupBox2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(624, 18);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(292, 271);
            this.groupBox2.TabIndex = 300;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Enviar Mensagem";
            // 
            // txtmensagem
            // 
            this.txtmensagem.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtmensagem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtmensagem.Location = new System.Drawing.Point(21, 109);
            this.txtmensagem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtmensagem.Multiline = true;
            this.txtmensagem.Name = "txtmensagem";
            this.txtmensagem.Size = new System.Drawing.Size(253, 82);
            this.txtmensagem.TabIndex = 299;
            this.txtmensagem.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 16);
            this.label1.TabIndex = 298;
            this.label1.Text = "Mandar Mensagem para o Cliente?";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // rdoNão
            // 
            this.rdoNão.AutoSize = true;
            this.rdoNão.Location = new System.Drawing.Point(81, 66);
            this.rdoNão.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdoNão.Name = "rdoNão";
            this.rdoNão.Size = new System.Drawing.Size(55, 20);
            this.rdoNão.TabIndex = 297;
            this.rdoNão.TabStop = true;
            this.rdoNão.Text = "Não";
            this.rdoNão.UseVisualStyleBackColor = true;
            this.rdoNão.CheckedChanged += new System.EventHandler(this.RdoNão_CheckedChanged);
            // 
            // rdoSim
            // 
            this.rdoSim.AutoSize = true;
            this.rdoSim.Location = new System.Drawing.Point(21, 66);
            this.rdoSim.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdoSim.Name = "rdoSim";
            this.rdoSim.Size = new System.Drawing.Size(54, 20);
            this.rdoSim.TabIndex = 296;
            this.rdoSim.TabStop = true;
            this.rdoSim.Text = "Sim";
            this.rdoSim.UseVisualStyleBackColor = true;
            this.rdoSim.CheckedChanged += new System.EventHandler(this.RdoSim_CheckedChanged);
            // 
            // btnEnviar
            // 
            this.btnEnviar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.btnEnviar.FlatAppearance.BorderSize = 0;
            this.btnEnviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnviar.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.btnEnviar.ForeColor = System.Drawing.Color.White;
            this.btnEnviar.Location = new System.Drawing.Point(21, 203);
            this.btnEnviar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(253, 50);
            this.btnEnviar.TabIndex = 165;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = false;
            this.btnEnviar.Visible = false;
            this.btnEnviar.Click += new System.EventHandler(this.Button2_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.textBox1.Location = new System.Drawing.Point(626, 356);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(292, 202);
            this.textBox1.TabIndex = 301;
            this.textBox1.Text = "Aqui e cadastrado o Cliente para ele  receba Promoções recados e muito mais, e a " +
    "loja tenha o controle.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Neon 80s", 16F);
            this.label3.Location = new System.Drawing.Point(621, 319);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 22);
            this.label3.TabIndex = 302;
            this.label3.Text = "Cliente";
            // 
            // FrmCadastarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmCadastarCliente";
            this.Size = new System.Drawing.Size(976, 609);
            this.Load += new System.EventHandler(this.FrmCadastarCliente_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtrua;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.ComboBox cboestado;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.TextBox txtdescri;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtmensagem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdoNão;
        private System.Windows.Forms.RadioButton rdoSim;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
    }
}
