﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.RH
{
    public partial class FrmConsultarFolhaDePagamento : UserControl
    {
        public FrmConsultarFolhaDePagamento()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {


            try
            {
                string nome = txtNome.Text;

                if (nome == string.Empty)
                {
                    MessageBox.Show("informe o nome do funcionario", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Database.RH.FolhaDePagamento.DatabaseFolhaDePagamento db = new Database.RH.FolhaDePagamento.DatabaseFolhaDePagamento();
                List<Database.Model.vw_consultar_folha> list = db.Consultar(nome);

                dgtConsulta.DataSource = list;
            }
            catch (Exception)
            {

                MessageBox.Show("informe o nome", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            

        }

        private void Button2_Click(object sender, EventArgs e)
        {


            try
            {
                Database.RH.FolhaDePagamento.DatabaseFolhaDePagamento db = new Database.RH.FolhaDePagamento.DatabaseFolhaDePagamento();
                List<Database.Model.vw_consultar_folha> list = db.ConsultarTODO();

                dgtConsulta.DataSource = list;
            }
            catch (Exception)
            {

                MessageBox.Show("informe o nome", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
    }
}
