﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.RH
{
    public partial class FrmCasdastrarFolhaDePagamento : UserControl
    {
        public FrmCasdastrarFolhaDePagamento()
        {
            InitializeComponent();
        }

        private void CarregarCombo()
        {
            try
            {
               
                Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
                List<Database.Model.tb_funcionario> list = db.tb_funcionario.ToList();

                list.Insert(0, new Database.Model.tb_funcionario { nm_funcionario = "" });

                // carrega as informações no combo 
                cboFuncionario.DisplayMember = nameof(Database.Model.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = list;
               
                //cbolistfuncionario.Items.Insert(0, "Selecione");
            }
            catch (Exception)
            {

                MessageBox.Show("Erro", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Horas Trabalhadas

                decimal SalarioNominal = Convert.ToDecimal(txtSalario.Text);

                int horastrabalhadasdia = 8 * 5;
                int HorasTrabalhadasMes = horastrabalhadasdia * 5;

                decimal ValorHoraTrabalhada = SalarioNominal / HorasTrabalhadasMes;

                //Atrasos 
                decimal HorasAtraso = ValorHoraTrabalhada * numericUpDown5.Value;

                //Faltas

                decimal faltas = SalarioNominal / 30;
                decimal valorfaltas = faltas * numericUpDown4.Value;

                //Valor Hora Extra Dia
                decimal PorcentagemHoraExtra = (ValorHoraTrabalhada * 50) / 100;
                decimal ValorHoraExtra = ValorHoraTrabalhada + PorcentagemHoraExtra;
                decimal HorasExtras = ValorHoraExtra * numericUpDown1.Value;

                if (HorasExtras >= 1)
                {
                    decimal horasextrasdia = HorasExtras;

                }
                else
                {
                    label29.Text = "0,00";
                }


                //Horas extras no feriado

                decimal PorcentagemHoraExtraferiado = (ValorHoraTrabalhada * 100) / 100;
                decimal ValorHoraExtraferiado = ValorHoraTrabalhada + PorcentagemHoraExtraferiado;
                decimal HorasExtrasFeriado = ValorHoraExtraferiado * numericUpDown2.Value;


                if (HorasExtrasFeriado >= 1)
                {
                    decimal horasextrasdomingo = HorasExtrasFeriado;

                }
                else
                {
                    label29.Text = "0,00";
                }



                decimal resultado = HorasExtras + HorasExtrasFeriado;
                resultado = Math.Round(resultado, 2);
                label29.Text = Convert.ToString(resultado);


                //Salario Base do INSS
                decimal SalarioBruto = SalarioNominal + HorasExtras - HorasAtraso - valorfaltas;
                decimal SalarioBaseINSS = SalarioBruto;

                if (SalarioBaseINSS <= 1659.38m)
                {
                    decimal salario = (SalarioBaseINSS * 8) / 100;
                    salario = Math.Round(salario, 2);
                    label40.Text = Convert.ToString(salario);
                }
                else if (SalarioBaseINSS >= 1659.39m && SalarioBaseINSS <= 2765.66m)
                {
                    decimal salario = (SalarioBaseINSS * 9) / 100;
                    salario = Math.Round(salario, 2);
                    label40.Text = Convert.ToString(salario);

                }
                else if (SalarioBaseINSS >= 2765.67m)
                {
                    decimal salario = (SalarioBaseINSS * 11) / 100;
                    salario = Math.Round(salario, 2);
                    label40.Text = Convert.ToString(salario);
                }

                //Imposto de Renda 
                decimal BaseCalculoIR = SalarioBaseINSS - Convert.ToDecimal(label40.Text);

                if (BaseCalculoIR <= 1903.98m)
                {
                    label42.Text = "0,00";

                }
                else if (BaseCalculoIR >= 1903.99m && BaseCalculoIR <= 2826.65m)
                {
                    decimal imposto = (BaseCalculoIR * 7.5m) / 100;
                    decimal reduzir = 142.80m;
                    reduzir = Math.Round(reduzir, 2);
                    label42.Text = Convert.ToString(reduzir);

                }
                else if (BaseCalculoIR >= 2826.66m && BaseCalculoIR <= 3751.05m)
                {
                    decimal imposto = (BaseCalculoIR * 15) / 100;
                    decimal reduzir = 354.80m;
                    reduzir = Math.Round(reduzir, 2);
                    label42.Text = Convert.ToString(reduzir);

                }
                else if (BaseCalculoIR >= 3751.06m && BaseCalculoIR <= 4664.68m)
                {
                    decimal imposto = (BaseCalculoIR * 22.5m) / 100;
                    decimal reduzir = 636.13m;
                    reduzir = Math.Round(reduzir, 2);
                    label42.Text = Convert.ToString(reduzir);
                }
                else if (BaseCalculoIR >= 4664.88m)
                {
                    decimal imposto = (BaseCalculoIR * 27.5m) / 100;
                    decimal reduzir = 869.36m;
                    reduzir = Math.Round(reduzir, 2);
                    label42.Text = Convert.ToString(reduzir);
                }

                //FGTS
                decimal fgts = (SalarioBaseINSS * 8) / 100;
                fgts = Math.Round(fgts, 2);
                label38.Text = Convert.ToString(fgts);


                //Salario Liquido
                decimal salarioliquido = SalarioNominal + Convert.ToDecimal(label29.Text) - HorasAtraso - valorfaltas - Convert.ToDecimal(label40.Text)
                                                        - Convert.ToDecimal(label42.Text) - Convert.ToDecimal(label30.Text) - Convert.ToDecimal(label38.Text);
                salarioliquido = Math.Round(salarioliquido, 2);
                label23.Text = Convert.ToString(salarioliquido);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ecolha um funcionario", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void CboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Model.tb_funcionario func = cboFuncionario.SelectedItem as Database.Model.tb_funcionario;


            txtID.Text = Convert.ToString(func.id_funcionario);
            label30.Text = Convert.ToString(func.vl_vt);
            label20.Text = Convert.ToString(func.vl_salario);
            label32.Text = Convert.ToString(func.vl_va);
            label34.Text = Convert.ToString(func.vl_vr);
            label36.Text = Convert.ToString(func.vl_convenio);
            txtSalario.Text = Convert.ToString(func.vl_salario);

            label45.Text = Convert.ToString("0,00");
        }

        private void CboFuncionario_Click(object sender, EventArgs e)
        {
            this.CarregarCombo();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_folha_pagamento A = new Database.Model.tb_folha_pagamento();
                A.fk_id_funcionario = Convert.ToInt32(txtID.Text);
                A.vl_bruto = Convert.ToDecimal(label20.Text);
                A.vl_convenio = Convert.ToDecimal(label36.Text);
                A.vl_fgts = Convert.ToDecimal(label38.Text);
                A.vl_horasextras = Convert.ToDecimal(numericUpDown1.Value);
                A.vl_inss = Convert.ToDecimal(label40.Text);
                A.vl_ir = Convert.ToDecimal(label42.Text);
                A.vl_liquido = Convert.ToDecimal(label23.Text);
                A.vl_salario = Convert.ToDecimal(label20.Text);
                A.vl_va = Convert.ToDecimal(label32.Text);
                A.vl_vt = Convert.ToDecimal(label30.Text);
                A.dt_pagamento = dateTimePicker1.Value.Date;
                //

                Business.RH.FolhaDePagamento.BusinessFolhaDePagamento B = new Business.RH.FolhaDePagamento.BusinessFolhaDePagamento();
                B.cadastrar(A);

                MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
