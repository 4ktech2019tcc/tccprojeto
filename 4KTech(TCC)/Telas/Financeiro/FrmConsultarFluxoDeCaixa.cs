﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Financeiro
{
    public partial class FrmConsultarFluxoDeCaixa : UserControl
    {
        public FrmConsultarFluxoDeCaixa()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //dgtConsulta.AutoGenerateColumns = false;

                DateTime data = dtpData.Value.Date;
                if (data == null)
                {
                    MessageBox.Show("informe a data", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                Database.Financeiro.FluxoDeCaixa.DatabaseFluxoDeCaixa db = new Database.Financeiro.FluxoDeCaixa.DatabaseFluxoDeCaixa();
                List<Database.Model.vw_consultar_fluxodecaixa> list = db.consultar(data);

                dgtConsulta.DataSource = list;
            }
            catch (Exception)
            {
                MessageBox.Show("Procure um administrador do sistema", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
               // dgtConsulta.AutoGenerateColumns = false;
                Database.Financeiro.FluxoDeCaixa.DatabaseFluxoDeCaixa db = new Database.Financeiro.FluxoDeCaixa.DatabaseFluxoDeCaixa();
                List<Database.Model.vw_consultar_fluxodecaixa> list = db.consultarPortudo();

                dgtConsulta.DataSource = list;
            }
            catch (Exception)
            {

                MessageBox.Show("Procure um administrador do sistema", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

       
    }
}
