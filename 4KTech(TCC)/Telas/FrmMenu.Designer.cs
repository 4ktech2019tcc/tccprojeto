﻿namespace _4KTech_TCC_.Telas
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timerMenu = new System.Windows.Forms.Timer(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.PanelDosBtn = new System.Windows.Forms.FlowLayoutPanel();
            this.panelb2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.panelb3 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.panelb4 = new System.Windows.Forms.Panel();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button22 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button21 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.frmProdutoCompraa1 = new _4KTech_TCC_.Telas.Vendas.frmProdutoCompraa();
            this.frmCadastrarVenda1 = new _4KTech_TCC_.Telas.Vendas.frmCadastrarVenda();
            this.frmCadastarCliente1 = new _4KTech_TCC_.Telas.Vendas.FrmCadastarCliente();
            this.frmAlterarClientee1 = new _4KTech_TCC_.Telas.Vendas.frmAlterarClientee();
            this.frmConsultarFolhaDePagamento1 = new _4KTech_TCC_.Telas.RH.FrmConsultarFolhaDePagamento();
            this.frmCasdastrarFolhaDePagamento1 = new _4KTech_TCC_.Telas.RH.FrmCasdastrarFolhaDePagamento();
            this.frmConsutarEstoque1 = new _4KTech_TCC_.Telas.Logistica.FrmConsutarEstoque();
            this.frmConsultarFuncionario1 = new _4KTech_TCC_.Telas.Funcionarios.frmConsultarFuncionario();
            this.frmCadastrarfuncionario1 = new _4KTech_TCC_.Telas.Funcionarios.frmCadastrarfuncionario();
            this.frmAlterarFuncionario1 = new _4KTech_TCC_.Telas.Funcionarios.frmAlterarFuncionario();
            this.frmConsultarFluxoDeCaixa1 = new _4KTech_TCC_.Telas.Financeiro.FrmConsultarFluxoDeCaixa();
            this.frmCadastrarProdutoConsultarr1 = new _4KTech_TCC_.Telas.Compras.frmCadastrarProdutoConsultarr();
            this.frmCadastrarPedido1 = new _4KTech_TCC_.Telas.Compras.frmCadastrarPedido();
            this.frmCadastrarFornecedores1 = new _4KTech_TCC_.Telas.Compras.frmCadastrarFornecedores();
            this.frmAlteraroProdutoo1 = new _4KTech_TCC_.Telas.Compras.frmAlteraroProdutoo();
            this.frmAlterarFornecedorr1 = new _4KTech_TCC_.Telas.Compras.frmAlterarFornecedorr();
            this.frmCRMsendMail1 = new _4KTech_TCC_.Telas.CRM.frmCRMsendMail();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.PanelDosBtn.SuspendLayout();
            this.panelb2.SuspendLayout();
            this.panelb3.SuspendLayout();
            this.panelb4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::_4KTech_TCC_.Properties.Resources.minimize_window_120px;
            this.pictureBox1.Location = new System.Drawing.Point(945, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click_1);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::_4KTech_TCC_.Properties.Resources.cancel_120_azul_px;
            this.pictureBox5.Location = new System.Drawing.Point(985, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(27, 28);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 8;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 10;
            this.timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 10;
            this.timer3.Tick += new System.EventHandler(this.Timer3_Tick);
            // 
            // timer4
            // 
            this.timer4.Interval = 10;
            this.timer4.Tick += new System.EventHandler(this.Timer4_Tick);
            // 
            // timerMenu
            // 
            this.timerMenu.Interval = 10;
            this.timerMenu.Tick += new System.EventHandler(this.TimerMenu_Tick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(42)))), ((int)(((byte)(54)))));
            this.panel3.Controls.Add(this.pictureBox4);
            this.panel3.Controls.Add(this.PanelDosBtn);
            this.panel3.Location = new System.Drawing.Point(41, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(191, 1500);
            this.panel3.TabIndex = 7;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::_4KTech_TCC_.Properties.Resources._4k_tech_logo_solo_branco;
            this.pictureBox4.Location = new System.Drawing.Point(7, 6);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(67, 31);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // PanelDosBtn
            // 
            this.PanelDosBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(42)))), ((int)(((byte)(54)))));
            this.PanelDosBtn.Controls.Add(this.panelb2);
            this.PanelDosBtn.Controls.Add(this.panelb3);
            this.PanelDosBtn.Controls.Add(this.panelb4);
            this.PanelDosBtn.Controls.Add(this.panel2);
            this.PanelDosBtn.Controls.Add(this.panel4);
            this.PanelDosBtn.Location = new System.Drawing.Point(4, 114);
            this.PanelDosBtn.Name = "PanelDosBtn";
            this.PanelDosBtn.Size = new System.Drawing.Size(182, 548);
            this.PanelDosBtn.TabIndex = 6;
            // 
            // panelb2
            // 
            this.panelb2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.panelb2.Controls.Add(this.button2);
            this.panelb2.Controls.Add(this.button3);
            this.panelb2.Controls.Add(this.button4);
            this.panelb2.Controls.Add(this.button5);
            this.panelb2.Controls.Add(this.button10);
            this.panelb2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.panelb2.Location = new System.Drawing.Point(3, 3);
            this.panelb2.MaximumSize = new System.Drawing.Size(175, 221);
            this.panelb2.MinimumSize = new System.Drawing.Size(175, 57);
            this.panelb2.Name = "panelb2";
            this.panelb2.Size = new System.Drawing.Size(175, 57);
            this.panelb2.TabIndex = 2;
            this.panelb2.Leave += new System.EventHandler(this.Panelb2_Leave);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(0, 180);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(175, 41);
            this.button2.TabIndex = 6;
            this.button2.Text = "Cadastrar produto";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(0, 139);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(175, 41);
            this.button3.TabIndex = 5;
            this.button3.Text = "Comprar Suprimentos";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(0, 98);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(175, 41);
            this.button4.TabIndex = 4;
            this.button4.Text = "Alterar fornecedor";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(0, 57);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(175, 41);
            this.button5.TabIndex = 3;
            this.button5.Text = "Cadastrar fornecedor";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.Dock = System.Windows.Forms.DockStyle.Top;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Image = global::_4KTech_TCC_.Properties.Resources.cardboard_box_20px;
            this.button10.Location = new System.Drawing.Point(0, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(175, 57);
            this.button10.TabIndex = 2;
            this.button10.Text = "Suprimentos";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // panelb3
            // 
            this.panelb3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.panelb3.Controls.Add(this.button11);
            this.panelb3.Controls.Add(this.button12);
            this.panelb3.Controls.Add(this.button13);
            this.panelb3.Controls.Add(this.button14);
            this.panelb3.Controls.Add(this.button15);
            this.panelb3.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.panelb3.Location = new System.Drawing.Point(3, 66);
            this.panelb3.MaximumSize = new System.Drawing.Size(175, 221);
            this.panelb3.MinimumSize = new System.Drawing.Size(175, 57);
            this.panelb3.Name = "panelb3";
            this.panelb3.Size = new System.Drawing.Size(175, 57);
            this.panelb3.TabIndex = 2;
            this.panelb3.Leave += new System.EventHandler(this.Panelb3_Leave);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button11.Dock = System.Windows.Forms.DockStyle.Top;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(0, 180);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(175, 41);
            this.button11.TabIndex = 6;
            this.button11.Text = "-----";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button12.Dock = System.Windows.Forms.DockStyle.Top;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(0, 139);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(175, 41);
            this.button12.TabIndex = 5;
            this.button12.Text = "Despesas(OFF)";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button13.Dock = System.Windows.Forms.DockStyle.Top;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(0, 98);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(175, 41);
            this.button13.TabIndex = 4;
            this.button13.Text = "Gastos (OFF)";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button14.Dock = System.Windows.Forms.DockStyle.Top;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(0, 57);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(175, 41);
            this.button14.TabIndex = 3;
            this.button14.Text = "Fluxo de Caixa";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.Button14_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.button15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button15.Dock = System.Windows.Forms.DockStyle.Top;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Image = global::_4KTech_TCC_.Properties.Resources.bonds_20px;
            this.button15.Location = new System.Drawing.Point(0, 0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(175, 57);
            this.button15.TabIndex = 2;
            this.button15.Text = "Financeiro    ";
            this.button15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button15.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.Button15_Click);
            // 
            // panelb4
            // 
            this.panelb4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.panelb4.Controls.Add(this.button16);
            this.panelb4.Controls.Add(this.button17);
            this.panelb4.Controls.Add(this.button18);
            this.panelb4.Controls.Add(this.button19);
            this.panelb4.Controls.Add(this.button20);
            this.panelb4.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.panelb4.Location = new System.Drawing.Point(3, 129);
            this.panelb4.MaximumSize = new System.Drawing.Size(175, 221);
            this.panelb4.MinimumSize = new System.Drawing.Size(175, 57);
            this.panelb4.Name = "panelb4";
            this.panelb4.Size = new System.Drawing.Size(175, 57);
            this.panelb4.TabIndex = 2;
            this.panelb4.Leave += new System.EventHandler(this.Panelb4_Leave);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button16.Dock = System.Windows.Forms.DockStyle.Top;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(0, 180);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(175, 41);
            this.button16.TabIndex = 6;
            this.button16.Text = "----";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button17.Dock = System.Windows.Forms.DockStyle.Top;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(0, 139);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(175, 41);
            this.button17.TabIndex = 5;
            this.button17.Text = "-----";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button18.Dock = System.Windows.Forms.DockStyle.Top;
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(0, 98);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(175, 41);
            this.button18.TabIndex = 4;
            this.button18.Text = "----";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button19.Dock = System.Windows.Forms.DockStyle.Top;
            this.button19.FlatAppearance.BorderSize = 0;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ForeColor = System.Drawing.Color.White;
            this.button19.Location = new System.Drawing.Point(0, 57);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(175, 41);
            this.button19.TabIndex = 3;
            this.button19.Text = "Consultar Estoque";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.button20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button20.Dock = System.Windows.Forms.DockStyle.Top;
            this.button20.FlatAppearance.BorderSize = 0;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Image = global::_4KTech_TCC_.Properties.Resources.fork_lift_20px;
            this.button20.Location = new System.Drawing.Point(0, 0);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(175, 57);
            this.button20.TabIndex = 2;
            this.button20.Text = "Estoque        ";
            this.button20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button20.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.Button20_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.button25);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.panel2.Location = new System.Drawing.Point(3, 192);
            this.panel2.MaximumSize = new System.Drawing.Size(175, 221);
            this.panel2.MinimumSize = new System.Drawing.Size(175, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(175, 57);
            this.panel2.TabIndex = 2;
            this.panel2.Leave += new System.EventHandler(this.Panel2_Leave);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(0, 221);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(175, 43);
            this.button7.TabIndex = 8;
            this.button7.Text = "Consultar Folha de Pagamento";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button25.Dock = System.Windows.Forms.DockStyle.Top;
            this.button25.FlatAppearance.BorderSize = 0;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(0, 180);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(175, 41);
            this.button25.TabIndex = 7;
            this.button25.Text = "Folha de Pagamento";
            this.button25.UseVisualStyleBackColor = false;
            this.button25.Click += new System.EventHandler(this.Button25_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 41);
            this.button1.TabIndex = 6;
            this.button1.Text = "Consultar Funcionarios";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(0, 98);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(175, 41);
            this.button6.TabIndex = 5;
            this.button6.Text = "Alterar e Remover Funcionario";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button8.Dock = System.Windows.Forms.DockStyle.Top;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(0, 57);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(175, 41);
            this.button8.TabIndex = 3;
            this.button8.Text = "Cadastrar Funcionario";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.Dock = System.Windows.Forms.DockStyle.Top;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Image = global::_4KTech_TCC_.Properties.Resources.parse_from_clipboard_20px;
            this.button9.Location = new System.Drawing.Point(0, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(175, 57);
            this.button9.TabIndex = 2;
            this.button9.Text = "R.H             ";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.panel4.Controls.Add(this.button22);
            this.panel4.Controls.Add(this.button26);
            this.panel4.Controls.Add(this.button27);
            this.panel4.Controls.Add(this.button29);
            this.panel4.Controls.Add(this.button30);
            this.panel4.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.panel4.Location = new System.Drawing.Point(3, 255);
            this.panel4.MaximumSize = new System.Drawing.Size(175, 221);
            this.panel4.MinimumSize = new System.Drawing.Size(175, 57);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(175, 57);
            this.panel4.TabIndex = 8;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button22.Dock = System.Windows.Forms.DockStyle.Top;
            this.button22.FlatAppearance.BorderSize = 0;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Location = new System.Drawing.Point(0, 182);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(175, 41);
            this.button22.TabIndex = 7;
            this.button22.Text = "Cadastrar Produto";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.Button22_Click_1);
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button26.Dock = System.Windows.Forms.DockStyle.Top;
            this.button26.FlatAppearance.BorderSize = 0;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.ForeColor = System.Drawing.Color.White;
            this.button26.Location = new System.Drawing.Point(0, 141);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(175, 41);
            this.button26.TabIndex = 6;
            this.button26.Text = "Alterar Cliente";
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.Button26_Click);
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button27.Dock = System.Windows.Forms.DockStyle.Top;
            this.button27.FlatAppearance.BorderSize = 0;
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button27.ForeColor = System.Drawing.Color.White;
            this.button27.Location = new System.Drawing.Point(0, 100);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(175, 41);
            this.button27.TabIndex = 5;
            this.button27.Text = "Cadastrar Cliente";
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.Button27_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(45)))), ((int)(((byte)(86)))));
            this.button29.Dock = System.Windows.Forms.DockStyle.Top;
            this.button29.FlatAppearance.BorderSize = 0;
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button29.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.ForeColor = System.Drawing.Color.White;
            this.button29.Location = new System.Drawing.Point(0, 59);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(175, 41);
            this.button29.TabIndex = 3;
            this.button29.Text = "Nova venda";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.Button29_Click);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.button30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button30.Dock = System.Windows.Forms.DockStyle.Top;
            this.button30.FlatAppearance.BorderSize = 0;
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button30.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Image = global::_4KTech_TCC_.Properties.Resources.fork_lift_20px;
            this.button30.Location = new System.Drawing.Point(0, 0);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(175, 59);
            this.button30.TabIndex = 2;
            this.button30.Text = "Vendas          ";
            this.button30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button30.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(82)))), ((int)(((byte)(138)))));
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MaximumSize = new System.Drawing.Size(278, 1500);
            this.panel1.MinimumSize = new System.Drawing.Size(42, 1500);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(42, 1500);
            this.panel1.TabIndex = 4;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(42)))), ((int)(((byte)(54)))));
            this.panel6.Controls.Add(this.button21);
            this.panel6.Location = new System.Drawing.Point(58, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(220, 1503);
            this.panel6.TabIndex = 8;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(25)))), ((int)(((byte)(33)))));
            this.button21.FlatAppearance.BorderSize = 0;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.button21.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button21.Image = global::_4KTech_TCC_.Properties.Resources.chrome_color_26px;
            this.button21.Location = new System.Drawing.Point(23, 83);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(174, 66);
            this.button21.TabIndex = 8;
            this.button21.Text = "Ir para o site ";
            this.button21.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.Button21_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::_4KTech_TCC_.Properties.Resources.menu_120px;
            this.pictureBox3.Location = new System.Drawing.Point(-11, 43);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(60, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.PictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::_4KTech_TCC_.Properties.Resources.home_120px;
            this.pictureBox2.Location = new System.Drawing.Point(-11, 83);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(63, 25);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.PictureBox2_Click);
            // 
            // timer5
            // 
            this.timer5.Interval = 10;
            this.timer5.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(113)))), ((int)(((byte)(125)))), ((int)(((byte)(126)))));
            this.panel5.Controls.Add(this.pictureBox5);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Location = new System.Drawing.Point(225, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1041, 31);
            this.panel5.TabIndex = 25;
            this.panel5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel5_MouseDown);
            this.panel5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel5_MouseMove);
            // 
            // frmProdutoCompraa1
            // 
            this.frmProdutoCompraa1.BackColor = System.Drawing.Color.White;
            this.frmProdutoCompraa1.Location = new System.Drawing.Point(261, 57);
            this.frmProdutoCompraa1.Name = "frmProdutoCompraa1";
            this.frmProdutoCompraa1.Size = new System.Drawing.Size(976, 609);
            this.frmProdutoCompraa1.TabIndex = 24;
            // 
            // frmCadastrarVenda1
            // 
            this.frmCadastrarVenda1.BackColor = System.Drawing.Color.White;
            this.frmCadastrarVenda1.Location = new System.Drawing.Point(261, 57);
            this.frmCadastrarVenda1.Name = "frmCadastrarVenda1";
            this.frmCadastrarVenda1.Size = new System.Drawing.Size(976, 609);
            this.frmCadastrarVenda1.TabIndex = 23;
            // 
            // frmCadastarCliente1
            // 
            this.frmCadastarCliente1.BackColor = System.Drawing.Color.White;
            this.frmCadastarCliente1.Location = new System.Drawing.Point(261, 57);
            this.frmCadastarCliente1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.frmCadastarCliente1.Name = "frmCadastarCliente1";
            this.frmCadastarCliente1.Size = new System.Drawing.Size(976, 609);
            this.frmCadastarCliente1.TabIndex = 22;
            // 
            // frmAlterarClientee1
            // 
            this.frmAlterarClientee1.BackColor = System.Drawing.Color.White;
            this.frmAlterarClientee1.Location = new System.Drawing.Point(261, 57);
            this.frmAlterarClientee1.Name = "frmAlterarClientee1";
            this.frmAlterarClientee1.Size = new System.Drawing.Size(976, 609);
            this.frmAlterarClientee1.TabIndex = 21;
            // 
            // frmConsultarFolhaDePagamento1
            // 
            this.frmConsultarFolhaDePagamento1.BackColor = System.Drawing.Color.White;
            this.frmConsultarFolhaDePagamento1.Location = new System.Drawing.Point(261, 57);
            this.frmConsultarFolhaDePagamento1.Name = "frmConsultarFolhaDePagamento1";
            this.frmConsultarFolhaDePagamento1.Size = new System.Drawing.Size(976, 609);
            this.frmConsultarFolhaDePagamento1.TabIndex = 20;
            // 
            // frmCasdastrarFolhaDePagamento1
            // 
            this.frmCasdastrarFolhaDePagamento1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.frmCasdastrarFolhaDePagamento1.Location = new System.Drawing.Point(261, 57);
            this.frmCasdastrarFolhaDePagamento1.Name = "frmCasdastrarFolhaDePagamento1";
            this.frmCasdastrarFolhaDePagamento1.Size = new System.Drawing.Size(976, 609);
            this.frmCasdastrarFolhaDePagamento1.TabIndex = 19;
            // 
            // frmConsutarEstoque1
            // 
            this.frmConsutarEstoque1.BackColor = System.Drawing.Color.White;
            this.frmConsutarEstoque1.Location = new System.Drawing.Point(261, 57);
            this.frmConsutarEstoque1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.frmConsutarEstoque1.Name = "frmConsutarEstoque1";
            this.frmConsutarEstoque1.Size = new System.Drawing.Size(976, 589);
            this.frmConsutarEstoque1.TabIndex = 18;
            // 
            // frmConsultarFuncionario1
            // 
            this.frmConsultarFuncionario1.BackColor = System.Drawing.Color.White;
            this.frmConsultarFuncionario1.Location = new System.Drawing.Point(261, 57);
            this.frmConsultarFuncionario1.Name = "frmConsultarFuncionario1";
            this.frmConsultarFuncionario1.Size = new System.Drawing.Size(976, 589);
            this.frmConsultarFuncionario1.TabIndex = 17;
            // 
            // frmCadastrarfuncionario1
            // 
            this.frmCadastrarfuncionario1.BackColor = System.Drawing.Color.White;
            this.frmCadastrarfuncionario1.Location = new System.Drawing.Point(261, 57);
            this.frmCadastrarfuncionario1.Name = "frmCadastrarfuncionario1";
            this.frmCadastrarfuncionario1.Size = new System.Drawing.Size(976, 609);
            this.frmCadastrarfuncionario1.TabIndex = 16;
            // 
            // frmAlterarFuncionario1
            // 
            this.frmAlterarFuncionario1.BackColor = System.Drawing.Color.White;
            this.frmAlterarFuncionario1.Location = new System.Drawing.Point(261, 57);
            this.frmAlterarFuncionario1.Name = "frmAlterarFuncionario1";
            this.frmAlterarFuncionario1.Size = new System.Drawing.Size(976, 589);
            this.frmAlterarFuncionario1.TabIndex = 15;
            // 
            // frmConsultarFluxoDeCaixa1
            // 
            this.frmConsultarFluxoDeCaixa1.BackColor = System.Drawing.Color.White;
            this.frmConsultarFluxoDeCaixa1.Location = new System.Drawing.Point(261, 57);
            this.frmConsultarFluxoDeCaixa1.Name = "frmConsultarFluxoDeCaixa1";
            this.frmConsultarFluxoDeCaixa1.Size = new System.Drawing.Size(976, 609);
            this.frmConsultarFluxoDeCaixa1.TabIndex = 14;
            // 
            // frmCadastrarProdutoConsultarr1
            // 
            this.frmCadastrarProdutoConsultarr1.BackColor = System.Drawing.Color.White;
            this.frmCadastrarProdutoConsultarr1.Location = new System.Drawing.Point(261, 57);
            this.frmCadastrarProdutoConsultarr1.Name = "frmCadastrarProdutoConsultarr1";
            this.frmCadastrarProdutoConsultarr1.Size = new System.Drawing.Size(976, 609);
            this.frmCadastrarProdutoConsultarr1.TabIndex = 13;
            // 
            // frmCadastrarPedido1
            // 
            this.frmCadastrarPedido1.BackColor = System.Drawing.Color.White;
            this.frmCadastrarPedido1.Location = new System.Drawing.Point(261, 57);
            this.frmCadastrarPedido1.Name = "frmCadastrarPedido1";
            this.frmCadastrarPedido1.Size = new System.Drawing.Size(976, 609);
            this.frmCadastrarPedido1.TabIndex = 12;
            // 
            // frmCadastrarFornecedores1
            // 
            this.frmCadastrarFornecedores1.BackColor = System.Drawing.Color.White;
            this.frmCadastrarFornecedores1.Location = new System.Drawing.Point(261, 57);
            this.frmCadastrarFornecedores1.Name = "frmCadastrarFornecedores1";
            this.frmCadastrarFornecedores1.Size = new System.Drawing.Size(976, 589);
            this.frmCadastrarFornecedores1.TabIndex = 11;
            // 
            // frmAlteraroProdutoo1
            // 
            this.frmAlteraroProdutoo1.BackColor = System.Drawing.Color.White;
            this.frmAlteraroProdutoo1.Location = new System.Drawing.Point(261, 57);
            this.frmAlteraroProdutoo1.Name = "frmAlteraroProdutoo1";
            this.frmAlteraroProdutoo1.Size = new System.Drawing.Size(976, 589);
            this.frmAlteraroProdutoo1.TabIndex = 10;
            // 
            // frmAlterarFornecedorr1
            // 
            this.frmAlterarFornecedorr1.BackColor = System.Drawing.Color.White;
            this.frmAlterarFornecedorr1.Location = new System.Drawing.Point(261, 57);
            this.frmAlterarFornecedorr1.Name = "frmAlterarFornecedorr1";
            this.frmAlterarFornecedorr1.Size = new System.Drawing.Size(976, 609);
            this.frmAlterarFornecedorr1.TabIndex = 9;
            // 
            // frmCRMsendMail1
            // 
            this.frmCRMsendMail1.BackColor = System.Drawing.Color.White;
            this.frmCRMsendMail1.Location = new System.Drawing.Point(261, 57);
            this.frmCRMsendMail1.Name = "frmCRMsendMail1";
            this.frmCRMsendMail1.Size = new System.Drawing.Size(976, 609);
            this.frmCRMsendMail1.TabIndex = 26;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(146)))), ((int)(((byte)(147)))));
            this.ClientSize = new System.Drawing.Size(1264, 688);
            this.Controls.Add(this.frmCRMsendMail1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.frmProdutoCompraa1);
            this.Controls.Add(this.frmCadastrarVenda1);
            this.Controls.Add(this.frmCadastarCliente1);
            this.Controls.Add(this.frmAlterarClientee1);
            this.Controls.Add(this.frmConsultarFolhaDePagamento1);
            this.Controls.Add(this.frmCasdastrarFolhaDePagamento1);
            this.Controls.Add(this.frmConsutarEstoque1);
            this.Controls.Add(this.frmConsultarFuncionario1);
            this.Controls.Add(this.frmCadastrarfuncionario1);
            this.Controls.Add(this.frmAlterarFuncionario1);
            this.Controls.Add(this.frmConsultarFluxoDeCaixa1);
            this.Controls.Add(this.frmCadastrarProdutoConsultarr1);
            this.Controls.Add(this.frmCadastrarPedido1);
            this.Controls.Add(this.frmCadastrarFornecedores1);
            this.Controls.Add(this.frmAlteraroProdutoo1);
            this.Controls.Add(this.frmAlterarFornecedorr1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMenu";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.PanelDosBtn.ResumeLayout(false);
            this.panelb2.ResumeLayout(false);
            this.panelb3.ResumeLayout(false);
            this.panelb4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Timer timerMenu;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button21;
        private Compras.frmAlterarFornecedorr frmAlterarFornecedorr1;
        private Compras.frmAlteraroProdutoo frmAlteraroProdutoo1;
        private Compras.frmCadastrarFornecedores frmCadastrarFornecedores1;
        private Compras.frmCadastrarPedido frmCadastrarPedido1;
        private Compras.frmCadastrarProdutoConsultarr frmCadastrarProdutoConsultarr1;
        private Financeiro.FrmConsultarFluxoDeCaixa frmConsultarFluxoDeCaixa1;
        private Funcionarios.frmAlterarFuncionario frmAlterarFuncionario1;
        private Funcionarios.frmCadastrarfuncionario frmCadastrarfuncionario1;
        private Funcionarios.frmConsultarFuncionario frmConsultarFuncionario1;
        private Logistica.FrmConsutarEstoque frmConsutarEstoque1;
        private RH.FrmCasdastrarFolhaDePagamento frmCasdastrarFolhaDePagamento1;
        private RH.FrmConsultarFolhaDePagamento frmConsultarFolhaDePagamento1;
        private Vendas.frmAlterarClientee frmAlterarClientee1;
        private Vendas.FrmCadastarCliente frmCadastarCliente1;
        private Vendas.frmCadastrarVenda frmCadastrarVenda1;
        private Vendas.frmProdutoCompraa frmProdutoCompraa1;
        private System.Windows.Forms.FlowLayoutPanel PanelDosBtn;
        private System.Windows.Forms.Panel panelb4;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Panel panelb3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Panel panelb2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel panel5;
        private CRM.frmCRMsendMail frmCRMsendMail1;
    }
}