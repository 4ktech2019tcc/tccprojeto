﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas
{
    public partial class FrmMenu : Form
    {
        int X = 0;
        int Y = 0;

        private bool isCollapsed;
        public FrmMenu()
        {
            InitializeComponent();
            //this.MouseDown += new MouseEventHandler(Panel5_MouseDown);
            //this.MouseMove += new MouseEventHandler(Panel5_MouseMove);

        }

        public void ocultarTelas()
        {

            ////ocultar as telas quando abre o programa;
            
            ////****** funcionario********
            
            ////alterar funcionario 
            frmAlterarFuncionario1.Hide();
            // cadastrar funcionario
            frmCadastrarfuncionario1.Hide();
            ////consultar funcionario
            frmConsultarFuncionario1.Hide();

            ////******cliente********
            ////cadastro cliente
            frmCadastarCliente1.Hide();
            ////alterração cliente
            frmAlterarClientee1.Hide();


            ////******Fornecedor********
            ////cadastro fornecedor
            frmCadastrarFornecedores1.Hide();
            ////Alterar Fornecedor
            frmAlterarFornecedorr1.Hide();

            ////*******Compras**********
            ////compra de produto
            frmCadastrarPedido1.Hide();
            ////Cadastro produto
            frmCadastrarProdutoConsultarr1.Hide();
            ////consultar estoque
            frmConsutarEstoque1.Hide();


            frmConsultarFluxoDeCaixa1.Hide();

            frmCasdastrarFolhaDePagamento1.Hide();

            frmConsultarFolhaDePagamento1.Hide();

            frmCadastrarVenda1.Hide();

            frmProdutoCompraa1.Hide();

            frmAlteraroProdutoo1.Hide();
            

        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                
                panel2.Height += 10;
                if (panel2.Size == panel2.MaximumSize)
                {
                    timer1.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panel2.Height -= 10;
                if (panel2.Size == panel2.MinimumSize)
                {
                    timer1.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panelb2.Height += 10;
                if (panelb2.Size == panelb2.MaximumSize)
                {
                    timer2.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panelb2.Height -= 10;
                if (panelb2.Size == panelb2.MinimumSize)
                {
                    timer2.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panelb3.Height += 10;
                if (panelb3.Size == panelb3.MaximumSize)
                {
                    timer3.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panelb3.Height -= 10;
                if (panelb3.Size == panelb3.MinimumSize)
                {
                    timer3.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void Timer4_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panelb4.Height += 10;
                if (panelb4.Size == panelb4.MaximumSize)
                {
                    timer4.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panelb4.Height -= 10;
                if (panelb4.Size == panelb4.MinimumSize)
                {
                    timer4.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void TimerMenu_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panel1.Width += 20;
                if (panel1.Size == panel1.MaximumSize)
                {
                    timerMenu.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panel1.Width -= 40;
                if (panel1.Size == panel1.MinimumSize)
                {
                    timerMenu.Stop();
                    isCollapsed = true;
                    panel3.Show();
                }
            }
        }
        

        private void Button10_Click(object sender, EventArgs e)
        {
            timer2.Start();
           
        }

        private void Button15_Click(object sender, EventArgs e)
        {
            timer3.Start();
           
        }

        private void Button20_Click(object sender, EventArgs e)
        {
            timer4.Start();
            
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            timer1.Start();
           
        }

        
       

        private void Panelb2_Leave(object sender, EventArgs e)
        {
            panelb2.Size = panelb2.MinimumSize;
        }

        private void Panelb3_Leave(object sender, EventArgs e)
        {
            panelb3.Size = panelb3.MinimumSize;
        }

        private void Panelb4_Leave(object sender, EventArgs e)
        {
            panelb4.Size = panelb4.MinimumSize;
        }

        private void Panel2_Leave(object sender, EventArgs e)
        {
            panel2.Size = panel2.MinimumSize;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult resposta = MessageBox.Show("Deseja realmente sair?", "4k tech",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

            if (resposta == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            timerMenu.Start();
            panel3.Hide();
        }

       

        

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            this.ocultarTelas();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
         
                frmCadastrarfuncionario1.Show();
                frmCadastrarfuncionario1.BringToFront(); 
           
        }

      

        private void Button25_Click(object sender, EventArgs e)
        {
            frmCasdastrarFolhaDePagamento1.Show();
            frmCasdastrarFolhaDePagamento1.BringToFront();
        }

      

        private void button5_Click(object sender, EventArgs e)
        {
          frmCadastrarFornecedores1.Show();
          frmCadastrarFornecedores1.BringToFront();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
          
                frmAlterarFuncionario1.Show();
                frmAlterarFuncionario1.BringToFront();
          

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario1.Show();
            frmConsultarFuncionario1.BringToFront();
           
        }

        private void button13_Click(object sender, EventArgs e)
        {
         
               frmCadastrarVenda1.Show();
               frmCadastrarVenda1.BringToFront();
          
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            DialogResult resposta = MessageBox.Show("Deseja realmente sair?", "4k tech",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

            if (resposta == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panel4.Height += 10;
                if (panel4.Size == panel4.MaximumSize)
                {
                    timer5.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panel4.Height -= 10;
                if (panel4.Size == panel4.MinimumSize)
                {
                    timer5.Stop();
                    isCollapsed = true;
                }
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            timer5.Start();
        }

        private void Button27_Click(object sender, EventArgs e)
        {
            frmCadastarCliente1.Show();
            frmCadastarCliente1.BringToFront();
           
        }

        private void Button26_Click(object sender, EventArgs e)
        {
          
          frmAlterarClientee1.Show();
            frmAlterarClientee1.BringToFront();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmAlterarFornecedorr1.Show();
           frmAlterarFornecedorr1.BringToFront();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            DialogResult resposta = MessageBox.Show("ir para o Menu Login?", "4k tech",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

            if (resposta == DialogResult.Yes)
            {
                frmLogin t = new frmLogin();
                t.Show();
                this.Hide();
            }
        }

        private void PictureBox1_Click_1(object sender, EventArgs e)
        {
            //minimizar a tela
            WindowState = FormWindowState.Minimized;
        }

        private void Button21_Click(object sender, EventArgs e)
        {
            // abre o google em uma pagina
            System.Diagnostics.Process.Start("chrome.exe", "https://www.facebook.com");
        }

        private void button3_Click(object sender, EventArgs e)
        {
           frmCadastrarPedido1.Show();
           frmCadastrarPedido1.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           frmCadastrarProdutoConsultarr1.Show();
           frmCadastrarProdutoConsultarr1.BringToFront(); 
        }

        private void button19_Click(object sender, EventArgs e)
        {
            frmConsutarEstoque1.Show();
            frmConsutarEstoque1.BringToFront();
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            frmConsultarFluxoDeCaixa1.Show();
            frmConsultarFluxoDeCaixa1.BringToFront();
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            frmConsultarFolhaDePagamento1.Show();
            frmConsultarFolhaDePagamento1.BringToFront();
        }

        private void Button29_Click(object sender, EventArgs e)
        {
            frmCadastrarVenda1.Show();
            frmCadastrarVenda1.BringToFront();
        }

      

        private void Button22_Click_1(object sender, EventArgs e)
        {
            frmProdutoCompraa1.Show();
            frmProdutoCompraa1.BringToFront();
        }

        private void Panel5_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            X = this.Left - MousePosition.X;
            Y = this.Top - MousePosition.Y;
        }

        private void Panel5_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            this.Left = X + MousePosition.X;
            this.Top = Y + MousePosition.Y;
        }
    }
}
