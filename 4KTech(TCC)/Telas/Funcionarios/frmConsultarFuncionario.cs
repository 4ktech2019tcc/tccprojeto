﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Funcionarios
{
    public partial class frmConsultarFuncionario : UserControl
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            dgtConsulta.AutoGenerateColumns = false;
            try
            {
                string nome = txtNome.Text.Trim();


                Business.funcionario.businessCadastroFuncionario busi = new Business.funcionario.businessCadastroFuncionario();
                List<Database.Model.tb_funcionario> list = busi.consultarPorNome(nome);
                dgtConsulta.DataSource = list;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }

        }

        private void FrmConsultarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            
        }
    }
}



           



