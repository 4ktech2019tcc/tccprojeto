﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Funcionarios
{
    public partial class frmCadastrarfuncionario : UserControl
    {
        public frmCadastrarfuncionario()
        {
            InitializeComponent();
        }

        public void loadCEP()
        {

            try
            {
                // pesquisa por CEP
                string cep = txtCEP.Text.Trim();

                Diferenciais.Correio.CorreioApi correio = new Diferenciais.Correio.CorreioApi();
                string endereço = correio.BuscarPorTudo(cep);

                txtEndereço.Text = endereço;
            }
            catch (Exception)
            {

                MessageBox.Show("informe o CEP", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //model funcionario
                Database.Model.tb_funcionario funcionario = new Database.Model.tb_funcionario();

                if (txtRG.MaskCompleted)
                {
                    funcionario.ds_RG = txtRG.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("RG é obrigatório");
                }

                if (txtCPF.MaskCompleted)
                {
                    funcionario.ds_CPF = txtCPF.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CPF é obrigatório");
                }

                //Criptografar senha
                string senha = txtSenha.Text.Trim();

                string chave = "1234567890123456";

                Diferenciais.Criptografia.AEScrip crip = new Diferenciais.Criptografia.AEScrip();

                string SenhaCriptografada = crip.Criptografar(chave,senha);

                
                funcionario.nm_funcionario = txtNome.Text.Trim();
                funcionario.ds_endereco = txtEndereço.Text;
                funcionario.vl_salario = nudSalario.Value;
                funcionario.vl_vt = nudValeTranporte.Value;
                funcionario.vl_va = nudValeAlimentaçao.Value;
                funcionario.vl_vr = nudValeRefeiçao.Value;
                funcionario.vl_convenio = nudConvenio.Value;
                funcionario.ds_usuario = txtuser.Text.Trim();
                funcionario.ds_senha = SenhaCriptografada;
                funcionario.bt_admin = ckbAdmin.Checked;
                funcionario.bt_funcionario = ckbFuncionario.Checked;
                funcionario.bt_permissao_RH = ckbRH.Checked;
                funcionario.bt_permissao_compras = ckbCompras.Checked;
                funcionario.bt_permissao_logistica = ckbLogistica.Checked;
                funcionario.bt_permissao_vendas= ckbVenda.Checked;
                funcionario.bt_permissao_financeiro = ckbFinanceiro.Checked;
                funcionario.ds_celular = txtcelular.Text.Trim();

                


                Business.funcionario.businessCadastroFuncionario busi = new Business.funcionario.businessCadastroFuncionario();
                busi.cadastrar(funcionario);

                MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Information);


                txtCEP.Text = null;
                txtNome.Text = null;
                txtEndereço.Text = null;
                nudSalario.Value = 0;
                nudValeTranporte.Value = 0;
                nudValeAlimentaçao.Value = 0;
                nudValeRefeiçao.Value = 0;
                nudConvenio.Value = 0;
                txtuser.Text = null;
                txtSenha.Text = null;
                ckbAdmin.Checked = false;
                ckbFuncionario.Checked = false;
                ckbRH.Checked = false;
                ckbCompras.Checked = false;
                ckbLogistica.Checked = false;
                ckbVenda.Checked = false;
                ckbFinanceiro.Checked = false;
                txtRG.Text = null;
                txtCPF.Text = null;
                txtcelular.Text = null;



            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Telas.Funcionarios.frmAlterarFuncionarioo tela = new frmAlterarFuncionarioo();
            //tela.Show();
        }

        

        private void TxtCEP_Leave(object sender, EventArgs e)
        {
            this.loadCEP();
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            ckbAdmin.Checked = true;
            ckbFuncionario.Checked = true;
            ckbRH.Checked = true;
            ckbCompras.Checked = true;
            ckbLogistica.Checked = true;
            ckbVenda.Checked = true;
            ckbFinanceiro.Checked = true;
        }
    }
}
