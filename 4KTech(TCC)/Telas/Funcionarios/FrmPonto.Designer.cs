﻿namespace _4KTech_TCC_.Telas.Funcionarios
{
    partial class FrmPonto
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSalvar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblIDfuncionario = new System.Windows.Forms.Label();
            this.lblNomeFuncionario = new System.Windows.Forms.Label();
            this.lblDiaAtual = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpSaida = new System.Windows.Forms.DateTimePicker();
            this.dtpSaidaAlmoço = new System.Windows.Forms.DateTimePicker();
            this.dtpEntradaAlmoço = new System.Windows.Forms.DateTimePicker();
            this.dtpEntrada = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.Transparent;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.btnSalvar.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.btnSalvar.Location = new System.Drawing.Point(230, 455);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(219, 31);
            this.btnSalvar.TabIndex = 6;
            this.btnSalvar.Text = "Registrar Ponto";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.BtnSalvar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.lblIDfuncionario);
            this.groupBox1.Controls.Add(this.lblNomeFuncionario);
            this.groupBox1.Controls.Add(this.lblDiaAtual);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(266, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 498);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // lblIDfuncionario
            // 
            this.lblIDfuncionario.AutoSize = true;
            this.lblIDfuncionario.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.lblIDfuncionario.ForeColor = System.Drawing.Color.White;
            this.lblIDfuncionario.Location = new System.Drawing.Point(143, 70);
            this.lblIDfuncionario.Name = "lblIDfuncionario";
            this.lblIDfuncionario.Size = new System.Drawing.Size(68, 16);
            this.lblIDfuncionario.TabIndex = 6;
            this.lblIDfuncionario.Text = "[Código]";
            // 
            // lblNomeFuncionario
            // 
            this.lblNomeFuncionario.AutoSize = true;
            this.lblNomeFuncionario.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.lblNomeFuncionario.ForeColor = System.Drawing.Color.White;
            this.lblNomeFuncionario.Location = new System.Drawing.Point(143, 37);
            this.lblNomeFuncionario.Name = "lblNomeFuncionario";
            this.lblNomeFuncionario.Size = new System.Drawing.Size(175, 16);
            this.lblNomeFuncionario.TabIndex = 5;
            this.lblNomeFuncionario.Text = "[Nome do Funcionário]";
            // 
            // lblDiaAtual
            // 
            this.lblDiaAtual.AutoSize = true;
            this.lblDiaAtual.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.lblDiaAtual.ForeColor = System.Drawing.Color.White;
            this.lblDiaAtual.Location = new System.Drawing.Point(346, 89);
            this.lblDiaAtual.Name = "lblDiaAtual";
            this.lblDiaAtual.Size = new System.Drawing.Size(86, 16);
            this.lblDiaAtual.TabIndex = 4;
            this.lblDiaAtual.Text = "[Dia Atual]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(61, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Código:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Funcionário:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.dtpSaida);
            this.groupBox2.Controls.Add(this.dtpSaidaAlmoço);
            this.groupBox2.Controls.Add(this.dtpEntradaAlmoço);
            this.groupBox2.Controls.Add(this.dtpEntrada);
            this.groupBox2.Location = new System.Drawing.Point(16, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(433, 337);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(128, 254);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 16);
            this.label11.TabIndex = 12;
            this.label11.Text = "Saída";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(46, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Entrada Almoço";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(77, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 16);
            this.label8.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(64, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "Saída Almoço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(110, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Entrada";
            // 
            // dtpSaida
            // 
            this.dtpSaida.CalendarFont = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSaida.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSaida.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpSaida.Location = new System.Drawing.Point(214, 254);
            this.dtpSaida.Name = "dtpSaida";
            this.dtpSaida.ShowUpDown = true;
            this.dtpSaida.Size = new System.Drawing.Size(158, 26);
            this.dtpSaida.TabIndex = 3;
            this.dtpSaida.Value = new System.DateTime(2018, 10, 4, 22, 19, 28, 0);
            // 
            // dtpSaidaAlmoço
            // 
            this.dtpSaidaAlmoço.CalendarFont = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSaidaAlmoço.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSaidaAlmoço.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpSaidaAlmoço.Location = new System.Drawing.Point(214, 185);
            this.dtpSaidaAlmoço.Name = "dtpSaidaAlmoço";
            this.dtpSaidaAlmoço.ShowUpDown = true;
            this.dtpSaidaAlmoço.Size = new System.Drawing.Size(158, 26);
            this.dtpSaidaAlmoço.TabIndex = 2;
            this.dtpSaidaAlmoço.Value = new System.DateTime(2018, 10, 4, 22, 19, 28, 0);
            // 
            // dtpEntradaAlmoço
            // 
            this.dtpEntradaAlmoço.CalendarFont = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntradaAlmoço.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntradaAlmoço.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEntradaAlmoço.Location = new System.Drawing.Point(214, 117);
            this.dtpEntradaAlmoço.Name = "dtpEntradaAlmoço";
            this.dtpEntradaAlmoço.ShowUpDown = true;
            this.dtpEntradaAlmoço.Size = new System.Drawing.Size(158, 26);
            this.dtpEntradaAlmoço.TabIndex = 1;
            this.dtpEntradaAlmoço.Value = new System.DateTime(2018, 10, 4, 22, 19, 28, 0);
            // 
            // dtpEntrada
            // 
            this.dtpEntrada.CalendarFont = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntrada.CustomFormat = "00:00:00";
            this.dtpEntrada.Enabled = false;
            this.dtpEntrada.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEntrada.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEntrada.Location = new System.Drawing.Point(214, 66);
            this.dtpEntrada.Name = "dtpEntrada";
            this.dtpEntrada.ShowUpDown = true;
            this.dtpEntrada.Size = new System.Drawing.Size(158, 26);
            this.dtpEntrada.TabIndex = 0;
            // 
            // FrmPonto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmPonto";
            this.Size = new System.Drawing.Size(976, 589);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblIDfuncionario;
        private System.Windows.Forms.Label lblNomeFuncionario;
        private System.Windows.Forms.Label lblDiaAtual;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpSaida;
        private System.Windows.Forms.DateTimePicker dtpSaidaAlmoço;
        private System.Windows.Forms.DateTimePicker dtpEntradaAlmoço;
        private System.Windows.Forms.DateTimePicker dtpEntrada;
    }
}
