﻿namespace _4KTech_TCC_.Telas.Funcionarios
{
    partial class frmAlterarFuncionario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.ckbAdmin = new System.Windows.Forms.CheckBox();
            this.ckbRH = new System.Windows.Forms.CheckBox();
            this.ckbCompras = new System.Windows.Forms.CheckBox();
            this.ckbVendas = new System.Windows.Forms.CheckBox();
            this.ckbLogistica = new System.Windows.Forms.CheckBox();
            this.ckbFinanceiro = new System.Windows.Forms.CheckBox();
            this.ckbFuncionario = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.cbolistfuncionario = new System.Windows.Forms.ComboBox();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.nudValeRefeiçao = new System.Windows.Forms.NumericUpDown();
            this.nudValeAlimentaçao = new System.Windows.Forms.NumericUpDown();
            this.txtEndereço = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudValeTranporte = new System.Windows.Forms.NumericUpDown();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeiçao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentaçao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTranporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.txtSenha);
            this.groupBox2.Controls.Add(this.ckbAdmin);
            this.groupBox2.Controls.Add(this.ckbRH);
            this.groupBox2.Controls.Add(this.ckbCompras);
            this.groupBox2.Controls.Add(this.ckbVendas);
            this.groupBox2.Controls.Add(this.ckbLogistica);
            this.groupBox2.Controls.Add(this.ckbFinanceiro);
            this.groupBox2.Controls.Add(this.ckbFuncionario);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtLogin);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(524, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(372, 398);
            this.groupBox2.TabIndex = 123;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acesso";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // txtSenha
            // 
            this.txtSenha.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtSenha.Location = new System.Drawing.Point(92, 50);
            this.txtSenha.MaxLength = 100;
            this.txtSenha.Multiline = true;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(259, 20);
            this.txtSenha.TabIndex = 101;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // ckbAdmin
            // 
            this.ckbAdmin.AutoSize = true;
            this.ckbAdmin.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbAdmin.ForeColor = System.Drawing.Color.Black;
            this.ckbAdmin.Location = new System.Drawing.Point(92, 102);
            this.ckbAdmin.Name = "ckbAdmin";
            this.ckbAdmin.Size = new System.Drawing.Size(75, 20);
            this.ckbAdmin.TabIndex = 12;
            this.ckbAdmin.Text = "Admin";
            this.ckbAdmin.UseVisualStyleBackColor = true;
            // 
            // ckbRH
            // 
            this.ckbRH.AutoSize = true;
            this.ckbRH.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbRH.ForeColor = System.Drawing.Color.Black;
            this.ckbRH.Location = new System.Drawing.Point(92, 166);
            this.ckbRH.Name = "ckbRH";
            this.ckbRH.Size = new System.Drawing.Size(46, 20);
            this.ckbRH.TabIndex = 14;
            this.ckbRH.Text = "RH";
            this.ckbRH.UseVisualStyleBackColor = true;
            // 
            // ckbCompras
            // 
            this.ckbCompras.AutoSize = true;
            this.ckbCompras.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbCompras.ForeColor = System.Drawing.Color.Black;
            this.ckbCompras.Location = new System.Drawing.Point(92, 192);
            this.ckbCompras.Name = "ckbCompras";
            this.ckbCompras.Size = new System.Drawing.Size(92, 20);
            this.ckbCompras.TabIndex = 15;
            this.ckbCompras.Text = "Compras";
            this.ckbCompras.UseVisualStyleBackColor = true;
            // 
            // ckbVendas
            // 
            this.ckbVendas.AutoSize = true;
            this.ckbVendas.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbVendas.ForeColor = System.Drawing.Color.Black;
            this.ckbVendas.Location = new System.Drawing.Point(92, 243);
            this.ckbVendas.Name = "ckbVendas";
            this.ckbVendas.Size = new System.Drawing.Size(81, 20);
            this.ckbVendas.TabIndex = 17;
            this.ckbVendas.Text = "Vendas";
            this.ckbVendas.UseVisualStyleBackColor = true;
            // 
            // ckbLogistica
            // 
            this.ckbLogistica.AutoSize = true;
            this.ckbLogistica.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbLogistica.ForeColor = System.Drawing.Color.Black;
            this.ckbLogistica.Location = new System.Drawing.Point(92, 218);
            this.ckbLogistica.Name = "ckbLogistica";
            this.ckbLogistica.Size = new System.Drawing.Size(90, 20);
            this.ckbLogistica.TabIndex = 16;
            this.ckbLogistica.Text = "Logística";
            this.ckbLogistica.UseVisualStyleBackColor = true;
            // 
            // ckbFinanceiro
            // 
            this.ckbFinanceiro.AutoSize = true;
            this.ckbFinanceiro.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.ckbFinanceiro.Location = new System.Drawing.Point(92, 270);
            this.ckbFinanceiro.Name = "ckbFinanceiro";
            this.ckbFinanceiro.Size = new System.Drawing.Size(100, 20);
            this.ckbFinanceiro.TabIndex = 18;
            this.ckbFinanceiro.Text = "Financeiro";
            this.ckbFinanceiro.UseVisualStyleBackColor = true;
            // 
            // ckbFuncionario
            // 
            this.ckbFuncionario.AutoSize = true;
            this.ckbFuncionario.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbFuncionario.ForeColor = System.Drawing.Color.Black;
            this.ckbFuncionario.Location = new System.Drawing.Point(92, 133);
            this.ckbFuncionario.Name = "ckbFuncionario";
            this.ckbFuncionario.Size = new System.Drawing.Size(110, 20);
            this.ckbFuncionario.TabIndex = 13;
            this.ckbFuncionario.Text = "Funcionário";
            this.ckbFuncionario.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(24, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 16);
            this.label18.TabIndex = 71;
            this.label18.Text = "Login:";
            // 
            // txtLogin
            // 
            this.txtLogin.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtLogin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtLogin.Location = new System.Drawing.Point(92, 20);
            this.txtLogin.MaxLength = 100;
            this.txtLogin.Multiline = true;
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(259, 24);
            this.txtLogin.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(21, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 16);
            this.label19.TabIndex = 100;
            this.label19.Text = "Senha:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtcelular);
            this.groupBox1.Controls.Add(this.cbolistfuncionario);
            this.groupBox1.Controls.Add(this.nudConvenio);
            this.groupBox1.Controls.Add(this.nudValeRefeiçao);
            this.groupBox1.Controls.Add(this.nudValeAlimentaçao);
            this.groupBox1.Controls.Add(this.txtEndereço);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nudValeTranporte);
            this.groupBox1.Controls.Add(this.nudSalario);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtCPF);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtRG);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(102, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 398);
            this.groupBox1.TabIndex = 122;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Funcionário";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(52, 124);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 16);
            this.label14.TabIndex = 291;
            this.label14.Text = "Cel:";
            // 
            // txtcelular
            // 
            this.txtcelular.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcelular.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtcelular.Location = new System.Drawing.Point(114, 121);
            this.txtcelular.Mask = "+55 (00) 00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(155, 19);
            this.txtcelular.TabIndex = 290;
            // 
            // cbolistfuncionario
            // 
            this.cbolistfuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbolistfuncionario.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.cbolistfuncionario.FormattingEnabled = true;
            this.cbolistfuncionario.Location = new System.Drawing.Point(114, 27);
            this.cbolistfuncionario.Name = "cbolistfuncionario";
            this.cbolistfuncionario.Size = new System.Drawing.Size(259, 24);
            this.cbolistfuncionario.TabIndex = 117;
            this.cbolistfuncionario.SelectedIndexChanged += new System.EventHandler(this.Cbolistfuncionario_SelectedIndexChanged);
            this.cbolistfuncionario.Click += new System.EventHandler(this.Cbolistfuncionario_Click);
            // 
            // nudConvenio
            // 
            this.nudConvenio.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudConvenio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudConvenio.Location = new System.Drawing.Point(114, 345);
            this.nudConvenio.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(130, 22);
            this.nudConvenio.TabIndex = 9;
            // 
            // nudValeRefeiçao
            // 
            this.nudValeRefeiçao.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudValeRefeiçao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudValeRefeiçao.DecimalPlaces = 2;
            this.nudValeRefeiçao.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeRefeiçao.Location = new System.Drawing.Point(114, 313);
            this.nudValeRefeiçao.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudValeRefeiçao.Name = "nudValeRefeiçao";
            this.nudValeRefeiçao.Size = new System.Drawing.Size(130, 22);
            this.nudValeRefeiçao.TabIndex = 8;
            // 
            // nudValeAlimentaçao
            // 
            this.nudValeAlimentaçao.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudValeAlimentaçao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudValeAlimentaçao.DecimalPlaces = 2;
            this.nudValeAlimentaçao.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeAlimentaçao.Location = new System.Drawing.Point(114, 281);
            this.nudValeAlimentaçao.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudValeAlimentaçao.Name = "nudValeAlimentaçao";
            this.nudValeAlimentaçao.Size = new System.Drawing.Size(130, 22);
            this.nudValeAlimentaçao.TabIndex = 7;
            // 
            // txtEndereço
            // 
            this.txtEndereço.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtEndereço.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEndereço.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtEndereço.Location = new System.Drawing.Point(114, 91);
            this.txtEndereço.MaxLength = 150;
            this.txtEndereço.Multiline = true;
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.Size = new System.Drawing.Size(259, 24);
            this.txtEndereço.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(11, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 100;
            this.label9.Text = "Endereço:";
            // 
            // nudValeTranporte
            // 
            this.nudValeTranporte.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudValeTranporte.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudValeTranporte.DecimalPlaces = 2;
            this.nudValeTranporte.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeTranporte.Location = new System.Drawing.Point(114, 249);
            this.nudValeTranporte.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudValeTranporte.Name = "nudValeTranporte";
            this.nudValeTranporte.Size = new System.Drawing.Size(131, 22);
            this.nudValeTranporte.TabIndex = 6;
            // 
            // nudSalario
            // 
            this.nudSalario.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudSalario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudSalario.Location = new System.Drawing.Point(114, 217);
            this.nudSalario.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(131, 22);
            this.nudSalario.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(53, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 116;
            this.label6.Text = "Cnv:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(57, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 16);
            this.label5.TabIndex = 114;
            this.label5.Text = "VR:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(58, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 112;
            this.label4.Text = "VA:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(60, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 110;
            this.label3.Text = "VT:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(31, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 16);
            this.label2.TabIndex = 108;
            this.label2.Text = "Salário:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(38, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 71;
            this.label1.Text = "Nome:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(38, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 71;
            this.label8.Text = "Nome:";
            // 
            // txtNome
            // 
            this.txtNome.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNome.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtNome.Location = new System.Drawing.Point(114, 61);
            this.txtNome.MaxLength = 150;
            this.txtNome.Multiline = true;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(259, 24);
            this.txtNome.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(49, 156);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 16);
            this.label12.TabIndex = 74;
            this.label12.Text = "CPF:";
            // 
            // txtCPF
            // 
            this.txtCPF.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtCPF.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCPF.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtCPF.Location = new System.Drawing.Point(114, 153);
            this.txtCPF.Mask = "000,000,000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(131, 19);
            this.txtCPF.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(55, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 16);
            this.label11.TabIndex = 73;
            this.label11.Text = "RG:";
            // 
            // txtRG
            // 
            this.txtRG.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtRG.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRG.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtRG.Location = new System.Drawing.Point(114, 185);
            this.txtRG.Mask = "00,000,000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(132, 19);
            this.txtRG.TabIndex = 3;
            this.txtRG.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Neon 80s", 10F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(102, 438);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(794, 45);
            this.button1.TabIndex = 124;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Neon 80s", 10F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(102, 495);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(398, 45);
            this.button2.TabIndex = 124;
            this.button2.Text = "Remover Funcionario";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // frmAlterarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmAlterarFuncionario";
            this.Size = new System.Drawing.Size(976, 589);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeiçao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentaçao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTranporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.CheckBox ckbAdmin;
        private System.Windows.Forms.CheckBox ckbRH;
        private System.Windows.Forms.CheckBox ckbCompras;
        private System.Windows.Forms.CheckBox ckbVendas;
        private System.Windows.Forms.CheckBox ckbLogistica;
        private System.Windows.Forms.CheckBox ckbFinanceiro;
        private System.Windows.Forms.CheckBox ckbFuncionario;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.NumericUpDown nudValeRefeiçao;
        private System.Windows.Forms.NumericUpDown nudValeAlimentaçao;
        private System.Windows.Forms.TextBox txtEndereço;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudValeTranporte;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbolistfuncionario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtcelular;
    }
}
