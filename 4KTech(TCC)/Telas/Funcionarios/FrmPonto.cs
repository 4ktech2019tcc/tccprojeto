﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Funcionarios
{
    public partial class FrmPonto : UserControl
    {
        public FrmPonto()
        {
            InitializeComponent();
            
            lblDiaAtual.Text = DateTime.Now.ToShortDateString();
            lblIDfuncionario.Text = Convert.ToString( Database.Logar_usuario.UsuarioLogado.IDUsuario);
            lblNomeFuncionario.Text = Database.Logar_usuario.UsuarioLogado.NomeUsuario;


        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_ponto p = new Database.Model.tb_ponto();

                int horas = (dtpEntrada.Value.Hour - dtpSaida.Value.Hour);
                int totalhoras = horas;

                p.dt_Ponto = Convert.ToDateTime(lblDiaAtual.Text);
                p.hr_almoco_ida = dtpEntradaAlmoço.Value;
                p.hr_almoco_ida = dtpEntradaAlmoço.Value;
                p.hr_almoco_volta = dtpSaidaAlmoço.Value;
                p.hr_saida = dtpSaida.Value;

                p.id_Funcionario = Convert.ToInt32(lblIDfuncionario.Text);


                Database.RH.Ponto.DatabasePonto ponto = new Database.RH.Ponto.DatabasePonto();
                ponto.Cadastrar(p);
            }
            catch (Exception)
            {

                MessageBox.Show("Complete os campos", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

         

               
        }
    }
}
