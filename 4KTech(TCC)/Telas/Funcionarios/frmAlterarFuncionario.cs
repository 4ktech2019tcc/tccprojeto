﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Funcionarios
{
    public partial class frmAlterarFuncionario : UserControl
    {
        public frmAlterarFuncionario()
        {
            InitializeComponent();
            //this.CarregarCombo();
        }


        private void SendMensagem()
        {

            try
            {
                string tel = txtcelular.Text.Trim();
                string men = "sua nova foi Alterada agora e:" + " " + txtSenha.Text;



                Diferenciais.SMS.SendWhatss send = new Diferenciais.SMS.SendWhatss();
                send.enviarWhatsapp(tel, men);

                Diferenciais.SMS.SMSAPI send2 = new Diferenciais.SMS.SMSAPI();
                send2.enviarSMS(tel, men);
            }
            catch (Exception)
            {

                
            }


        }

        private void CarregarCombo()
        {
            try
            {
                Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
                List<Database.Model.tb_funcionario> list = db.tb_funcionario.ToList();

                list.Insert(0, new Database.Model.tb_funcionario { nm_funcionario = "" });

                // carrega as informações no combo 
                cbolistfuncionario.DisplayMember = nameof(Database.Model.tb_funcionario.nm_funcionario);
                cbolistfuncionario.DataSource = list;

                //cbolistfuncionario.Items.Insert(0, "Selecione");
            }
            catch (Exception)
            {

                MessageBox.Show("Erro", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult resposta = MessageBox.Show("Deseja realmente fazer essa Alteração?", "4k tech",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {

                    string senha = txtSenha.Text.Trim();
                    //Chave da Criptografia
                    string chave = "1234567890123456";

                    Diferenciais.Criptografia.AEScrip crip = new Diferenciais.Criptografia.AEScrip();

                    string SenhaCriptografada = crip.Criptografar(chave, senha);

                    // chama o modelo 
                    Database.Model.tb_funcionario funcionario = cbolistfuncionario.SelectedItem as Database.Model.tb_funcionario;
                    int id = funcionario.id_funcionario;
                    // passa os parâmetros
                    Database.Model.tb_funcionario f = new Database.Model.tb_funcionario();
                    f.id_funcionario = id;
                    f.nm_funcionario = txtNome.Text.Trim();
                    f.ds_endereco = txtEndereço.Text.Trim();
                    f.vl_salario = nudSalario.Value;
                    f.vl_vt = nudValeTranporte.Value;
                    f.vl_va = nudValeAlimentaçao.Value;
                    f.vl_vr = nudValeRefeiçao.Value;
                    f.vl_convenio = nudConvenio.Value;
                    f.ds_usuario = txtLogin.Text.Trim();
                    f.ds_celular = txtcelular.Text.Trim();
                    //criptografar
                    f.ds_senha = SenhaCriptografada;

                    f.bt_admin = ckbAdmin.Checked;
                                                           
                    f.bt_funcionario = ckbFuncionario.Checked;
                    f.bt_permissao_RH = ckbRH.Checked;
                    f.bt_permissao_compras = ckbCompras.Checked;
                    f.bt_permissao_logistica = ckbLogistica.Checked;
                    f.bt_permissao_vendas = ckbVendas.Checked;
                    f.bt_permissao_financeiro = ckbFinanceiro.Checked;
                    f.ds_RG = txtRG.Text;
                    f.ds_CPF = txtCPF.Text;

                    //Envia a nova Senha Do FUncionario
                    this.SendMensagem();

                   
                    //MessageBox.Show("mensagem enviada", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Database.funcionario.businessAlterarFuncionario busi = new Database.funcionario.businessAlterarFuncionario();
                    busi.alterar(f);

                    MessageBox.Show("Alterado com sucesso", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Cbolistfuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
            Database.Model.tb_funcionario funcionario = cbolistfuncionario.SelectedItem as Database.Model.tb_funcionario;
            string nome = funcionario.nm_funcionario;

            Database.funcionario.DatabasefuncionarioConsulta f = new Database.funcionario.DatabasefuncionarioConsulta();
            List<Database.Model.tb_funcionario> list = f.consul(nome);
            txtNome.Text = funcionario.nm_funcionario;
            txtEndereço.Text = funcionario.ds_endereco;
            nudSalario.Value = funcionario.vl_salario;
            nudValeTranporte.Value = Convert.ToInt32(funcionario.vl_vt);
            nudValeAlimentaçao.Value = Convert.ToInt32(funcionario.vl_va);
            nudValeRefeiçao.Value = Convert.ToInt32(funcionario.vl_vr);
            nudConvenio.Value = Convert.ToInt32(funcionario.vl_convenio);
            txtLogin.Text = funcionario.ds_usuario;
            txtSenha.Text = funcionario.ds_senha;
            ckbAdmin.Checked = Convert.ToBoolean(funcionario.bt_admin);
            ckbFuncionario.Checked = Convert.ToBoolean(funcionario.bt_funcionario);
            ckbRH.Checked = Convert.ToBoolean(funcionario.bt_permissao_RH);
            ckbCompras.Checked = Convert.ToBoolean(funcionario.bt_permissao_compras);
            ckbLogistica.Checked = Convert.ToBoolean(funcionario.bt_permissao_logistica);
            ckbVendas.Checked = Convert.ToBoolean(funcionario.bt_permissao_vendas);
            ckbFinanceiro.Checked = Convert.ToBoolean(funcionario.bt_permissao_financeiro);
            txtCPF.Text = funcionario.ds_CPF;
            txtRG.Text = funcionario.ds_RG;
            txtcelular.Text = funcionario.ds_celular;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Cbolistfuncionario_Click(object sender, EventArgs e)
        {
            this.CarregarCombo();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult resposta = MessageBox.Show("Deseja realmente Remover esse funcionario?", "4k tech",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    DialogResult resposta2 = MessageBox.Show("Esse Operação não pode ser desfeita, deseja continuar?", "4k tech",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);

                    if (resposta == DialogResult.Yes)
                    {
                        Database.Model.tb_funcionario funcionario = cbolistfuncionario.SelectedItem as Database.Model.tb_funcionario;
                        int id = funcionario.id_funcionario;

                        Database.funcionario.FuncionarioDatabase db = new Database.funcionario.FuncionarioDatabase();
                        db.Remover(id);

                        MessageBox.Show("funcionario removido", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }


        }
    }
}
