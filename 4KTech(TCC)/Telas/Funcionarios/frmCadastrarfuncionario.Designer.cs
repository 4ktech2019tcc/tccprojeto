﻿namespace _4KTech_TCC_.Telas.Funcionarios
{
    partial class frmCadastrarfuncionario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.ckbAdmin = new System.Windows.Forms.CheckBox();
            this.ckbRH = new System.Windows.Forms.CheckBox();
            this.ckbCompras = new System.Windows.Forms.CheckBox();
            this.ckbVenda = new System.Windows.Forms.CheckBox();
            this.ckbLogistica = new System.Windows.Forms.CheckBox();
            this.ckbFinanceiro = new System.Windows.Forms.CheckBox();
            this.ckbFuncionario = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.nudValeRefeiçao = new System.Windows.Forms.NumericUpDown();
            this.nudValeAlimentaçao = new System.Windows.Forms.NumericUpDown();
            this.txtEndereço = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudValeTranporte = new System.Windows.Forms.NumericUpDown();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeiçao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentaçao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTranporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.txtSenha);
            this.groupBox2.Controls.Add(this.ckbAdmin);
            this.groupBox2.Controls.Add(this.ckbRH);
            this.groupBox2.Controls.Add(this.ckbCompras);
            this.groupBox2.Controls.Add(this.ckbVenda);
            this.groupBox2.Controls.Add(this.ckbLogistica);
            this.groupBox2.Controls.Add(this.ckbFinanceiro);
            this.groupBox2.Controls.Add(this.ckbFuncionario);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtuser);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(517, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(394, 474);
            this.groupBox2.TabIndex = 123;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acesso";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Neon 80s", 10F);
            this.button2.Location = new System.Drawing.Point(92, 361);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(256, 39);
            this.button2.TabIndex = 124;
            this.button2.Text = "Dar Todas as permissões ";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click_1);
            // 
            // txtSenha
            // 
            this.txtSenha.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSenha.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtSenha.Location = new System.Drawing.Point(92, 50);
            this.txtSenha.MaxLength = 100;
            this.txtSenha.Multiline = true;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(259, 20);
            this.txtSenha.TabIndex = 101;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // ckbAdmin
            // 
            this.ckbAdmin.AutoSize = true;
            this.ckbAdmin.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbAdmin.ForeColor = System.Drawing.Color.Black;
            this.ckbAdmin.Location = new System.Drawing.Point(92, 159);
            this.ckbAdmin.Name = "ckbAdmin";
            this.ckbAdmin.Size = new System.Drawing.Size(75, 20);
            this.ckbAdmin.TabIndex = 12;
            this.ckbAdmin.Text = "Admin";
            this.ckbAdmin.UseVisualStyleBackColor = true;
            // 
            // ckbRH
            // 
            this.ckbRH.AutoSize = true;
            this.ckbRH.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbRH.ForeColor = System.Drawing.Color.Black;
            this.ckbRH.Location = new System.Drawing.Point(92, 223);
            this.ckbRH.Name = "ckbRH";
            this.ckbRH.Size = new System.Drawing.Size(46, 20);
            this.ckbRH.TabIndex = 14;
            this.ckbRH.Text = "RH";
            this.ckbRH.UseVisualStyleBackColor = true;
            // 
            // ckbCompras
            // 
            this.ckbCompras.AutoSize = true;
            this.ckbCompras.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbCompras.ForeColor = System.Drawing.Color.Black;
            this.ckbCompras.Location = new System.Drawing.Point(92, 249);
            this.ckbCompras.Name = "ckbCompras";
            this.ckbCompras.Size = new System.Drawing.Size(92, 20);
            this.ckbCompras.TabIndex = 15;
            this.ckbCompras.Text = "Compras";
            this.ckbCompras.UseVisualStyleBackColor = true;
            // 
            // ckbVenda
            // 
            this.ckbVenda.AutoSize = true;
            this.ckbVenda.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbVenda.ForeColor = System.Drawing.Color.Black;
            this.ckbVenda.Location = new System.Drawing.Point(92, 300);
            this.ckbVenda.Name = "ckbVenda";
            this.ckbVenda.Size = new System.Drawing.Size(81, 20);
            this.ckbVenda.TabIndex = 17;
            this.ckbVenda.Text = "Vendas";
            this.ckbVenda.UseVisualStyleBackColor = true;
            // 
            // ckbLogistica
            // 
            this.ckbLogistica.AutoSize = true;
            this.ckbLogistica.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbLogistica.ForeColor = System.Drawing.Color.Black;
            this.ckbLogistica.Location = new System.Drawing.Point(92, 275);
            this.ckbLogistica.Name = "ckbLogistica";
            this.ckbLogistica.Size = new System.Drawing.Size(90, 20);
            this.ckbLogistica.TabIndex = 16;
            this.ckbLogistica.Text = "Logística";
            this.ckbLogistica.UseVisualStyleBackColor = true;
            // 
            // ckbFinanceiro
            // 
            this.ckbFinanceiro.AutoSize = true;
            this.ckbFinanceiro.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.ckbFinanceiro.Location = new System.Drawing.Point(92, 327);
            this.ckbFinanceiro.Name = "ckbFinanceiro";
            this.ckbFinanceiro.Size = new System.Drawing.Size(100, 20);
            this.ckbFinanceiro.TabIndex = 18;
            this.ckbFinanceiro.Text = "Financeiro";
            this.ckbFinanceiro.UseVisualStyleBackColor = true;
            // 
            // ckbFuncionario
            // 
            this.ckbFuncionario.AutoSize = true;
            this.ckbFuncionario.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.ckbFuncionario.ForeColor = System.Drawing.Color.Black;
            this.ckbFuncionario.Location = new System.Drawing.Point(92, 190);
            this.ckbFuncionario.Name = "ckbFuncionario";
            this.ckbFuncionario.Size = new System.Drawing.Size(110, 20);
            this.ckbFuncionario.TabIndex = 13;
            this.ckbFuncionario.Text = "Funcionário";
            this.ckbFuncionario.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(24, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 16);
            this.label18.TabIndex = 71;
            this.label18.Text = "Login:";
            // 
            // txtuser
            // 
            this.txtuser.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtuser.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtuser.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtuser.Location = new System.Drawing.Point(92, 20);
            this.txtuser.MaxLength = 100;
            this.txtuser.Multiline = true;
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(259, 24);
            this.txtuser.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(21, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 16);
            this.label19.TabIndex = 100;
            this.label19.Text = "Senha:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtcelular);
            this.groupBox1.Controls.Add(this.nudConvenio);
            this.groupBox1.Controls.Add(this.nudValeRefeiçao);
            this.groupBox1.Controls.Add(this.nudValeAlimentaçao);
            this.groupBox1.Controls.Add(this.txtEndereço);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nudValeTranporte);
            this.groupBox1.Controls.Add(this.nudSalario);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCEP);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtCPF);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtRG);
            this.groupBox1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(95, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 474);
            this.groupBox1.TabIndex = 122;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Cadastrar Funcionário";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(64, 193);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 16);
            this.label14.TabIndex = 291;
            this.label14.Text = "Cel:";
            // 
            // txtcelular
            // 
            this.txtcelular.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtcelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcelular.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtcelular.Location = new System.Drawing.Point(121, 190);
            this.txtcelular.Mask = "+55 (00) 00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(155, 19);
            this.txtcelular.TabIndex = 290;
            // 
            // nudConvenio
            // 
            this.nudConvenio.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudConvenio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudConvenio.Location = new System.Drawing.Point(121, 414);
            this.nudConvenio.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(130, 22);
            this.nudConvenio.TabIndex = 126;
            // 
            // nudValeRefeiçao
            // 
            this.nudValeRefeiçao.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudValeRefeiçao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudValeRefeiçao.DecimalPlaces = 2;
            this.nudValeRefeiçao.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeRefeiçao.Location = new System.Drawing.Point(121, 382);
            this.nudValeRefeiçao.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudValeRefeiçao.Name = "nudValeRefeiçao";
            this.nudValeRefeiçao.Size = new System.Drawing.Size(130, 22);
            this.nudValeRefeiçao.TabIndex = 125;
            // 
            // nudValeAlimentaçao
            // 
            this.nudValeAlimentaçao.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudValeAlimentaçao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudValeAlimentaçao.DecimalPlaces = 2;
            this.nudValeAlimentaçao.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeAlimentaçao.Location = new System.Drawing.Point(121, 350);
            this.nudValeAlimentaçao.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudValeAlimentaçao.Name = "nudValeAlimentaçao";
            this.nudValeAlimentaçao.Size = new System.Drawing.Size(130, 22);
            this.nudValeAlimentaçao.TabIndex = 124;
            // 
            // txtEndereço
            // 
            this.txtEndereço.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtEndereço.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEndereço.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtEndereço.Location = new System.Drawing.Point(121, 114);
            this.txtEndereço.MaxLength = 150;
            this.txtEndereço.Multiline = true;
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.ReadOnly = true;
            this.txtEndereço.Size = new System.Drawing.Size(259, 67);
            this.txtEndereço.TabIndex = 121;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(18, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 131;
            this.label9.Text = "Endereço:";
            // 
            // nudValeTranporte
            // 
            this.nudValeTranporte.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudValeTranporte.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudValeTranporte.DecimalPlaces = 2;
            this.nudValeTranporte.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeTranporte.Location = new System.Drawing.Point(121, 318);
            this.nudValeTranporte.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudValeTranporte.Name = "nudValeTranporte";
            this.nudValeTranporte.Size = new System.Drawing.Size(131, 22);
            this.nudValeTranporte.TabIndex = 123;
            // 
            // nudSalario
            // 
            this.nudSalario.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.nudSalario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudSalario.Location = new System.Drawing.Point(121, 286);
            this.nudSalario.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(131, 22);
            this.nudSalario.TabIndex = 122;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(60, 414);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 136;
            this.label6.Text = "Cnv:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(64, 380);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 16);
            this.label5.TabIndex = 135;
            this.label5.Text = "VR:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(65, 346);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 134;
            this.label4.Text = "VA:";
            // 
            // txtCEP
            // 
            this.txtCEP.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtCEP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCEP.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtCEP.Location = new System.Drawing.Point(121, 79);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(132, 19);
            this.txtCEP.TabIndex = 120;
            this.txtCEP.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.txtCEP.Leave += new System.EventHandler(this.TxtCEP_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(59, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 16);
            this.label7.TabIndex = 128;
            this.label7.Text = "CEP:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(67, 316);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 133;
            this.label3.Text = "VT:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(38, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 16);
            this.label2.TabIndex = 132;
            this.label2.Text = "Salário:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(54, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 127;
            this.label8.Text = "Nome:";
            // 
            // txtNome
            // 
            this.txtNome.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNome.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtNome.Location = new System.Drawing.Point(121, 40);
            this.txtNome.MaxLength = 150;
            this.txtNome.Multiline = true;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(259, 24);
            this.txtNome.TabIndex = 117;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(56, 226);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 130;
            this.label1.Text = "CPF:";
            // 
            // txtCPF
            // 
            this.txtCPF.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtCPF.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCPF.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtCPF.Location = new System.Drawing.Point(121, 222);
            this.txtCPF.Mask = "000,000,000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(131, 19);
            this.txtCPF.TabIndex = 118;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(62, 258);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 16);
            this.label10.TabIndex = 129;
            this.label10.Text = "RG:";
            // 
            // txtRG
            // 
            this.txtRG.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtRG.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRG.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtRG.Location = new System.Drawing.Point(121, 254);
            this.txtRG.Mask = "00,000,000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(132, 19);
            this.txtRG.TabIndex = 119;
            this.txtRG.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(41)))), ((int)(((byte)(94)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Neon 80s", 10F);
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Location = new System.Drawing.Point(95, 533);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(816, 39);
            this.button1.TabIndex = 124;
            this.button1.Text = "Cadastrar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // frmCadastrarfuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmCadastrarfuncionario";
            this.Size = new System.Drawing.Size(976, 609);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeiçao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentaçao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTranporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.CheckBox ckbAdmin;
        private System.Windows.Forms.CheckBox ckbRH;
        private System.Windows.Forms.CheckBox ckbCompras;
        private System.Windows.Forms.CheckBox ckbVenda;
        private System.Windows.Forms.CheckBox ckbLogistica;
        private System.Windows.Forms.CheckBox ckbFinanceiro;
        private System.Windows.Forms.CheckBox ckbFuncionario;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.NumericUpDown nudValeRefeiçao;
        private System.Windows.Forms.NumericUpDown nudValeAlimentaçao;
        private System.Windows.Forms.TextBox txtEndereço;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudValeTranporte;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.Button button2;
    }
}
