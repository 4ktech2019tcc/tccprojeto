﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Compras
{
    public partial class frmAlteraroProdutoo : UserControl
    {
        public frmAlteraroProdutoo()
        {
            InitializeComponent();
        }

        private void CarregarCombo()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.ToList();

            list.Insert(0, new Database.Model.tb_produto_compra { nm_produto = "" });

            // carrega as informações no combo 
            cboProduto.DisplayMember = nameof(Database.Model.tb_produto_compra.nm_produto);
            cboProduto.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_produto_compra A = cboProduto.SelectedItem as Database.Model.tb_produto_compra;
                int id = A.id_produto_compra;
                Database.Model.tb_produto_compra B = new Database.Model.tb_produto_compra();

                B.id_produto_compra = id;
                B.nm_produto = txtNome.Text.Trim();
                B.nm_marca = txtCategoria.Text.Trim();
                B.vl_preco = nudPreço.Value;
                B.vl_quantidade =1;
                Business.Compras.Produto.BusinessProduto C = new Business.Compras.Produto.BusinessProduto();
                C.AlterarProduto(B);
                MessageBox.Show("Alterado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void CboProduto_Click(object sender, EventArgs e)
        {
            this.CarregarCombo();
        }

        private void CboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Model.tb_produto_compra A = cboProduto.SelectedItem as Database.Model.tb_produto_compra;
            string nome = A.nm_produto;
            Database.funcionario.DatabasefuncionarioConsulta f = new Database.funcionario.DatabasefuncionarioConsulta();
            List<Database.Model.tb_produto_compra> list = f.consulPro(nome);
            txtNome.Text = A.nm_produto;
            txtCategoria.Text = A.nm_marca;
            nudPreço.Value = A.vl_preco;
            nudquantidade.Value = A.vl_quantidade;
        }
    }
}
