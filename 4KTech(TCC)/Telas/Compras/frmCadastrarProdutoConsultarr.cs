﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Compras
{
    public partial class frmCadastrarProdutoConsultarr : UserControl
    {
        public frmCadastrarProdutoConsultarr()
        {
            InitializeComponent();
        }


        public void care()
        {

            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.ToList();

            //list.Insert(0, new Database.Model.tb_produto_compra { nm_produto = "" });

            // carrega as informações no combo 
            cboProduto2.DisplayMember = nameof(Database.Model.tb_produto_compra.nm_produto);
            cboProduto2.DataSource = list;



        }

            //cbolistfuncionario.Items.Insert(0, "Selecione");
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_produto_compra A = new Database.Model.tb_produto_compra();
                A.vl_quantidade = Convert.ToInt32(nudQuantidade.Value);
                A.nm_produto = txtNome.Text;
                A.vl_preco = nudPreço.Value;
                A.nm_marca = txtCategoria.Text;

                Business.Compras.Produto.BusinessProduto C = new Business.Compras.Produto.BusinessProduto();
                C.CadastrarProduto(A);

                MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNomeProd.Text.Trim();
                Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
                List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.Where(f => f.nm_produto.Contains( nome)).ToList();
                dtgConsultar.DataSource = list;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            try
            {
                
                Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
                List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.ToList();
                dtgConsultar.DataSource = list;
            }
            catch (ArgumentException ex)
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Database.Model.tb_produto_compra p = cboProduto2.SelectedItem as Database.Model.tb_produto_compra;
            int id = p.id_produto_compra;

            Database.Model.tb_produto_compra A = new Database.Model.tb_produto_compra();
            A.vl_quantidade = 1;
            A.id_produto_compra = id;
            A.nm_produto = txtnome2.Text;
            A.vl_preco = nudpreco2.Value;

            Business.Compras.Produto.BusinessProduto busi = new Business.Compras.Produto.BusinessProduto();
            busi.AlterarProduto(A);

            MessageBox.Show("Alterado com sucesso", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void cboProduto2_Click(object sender, EventArgs e)
        {
            this.care();
        }

        private void cboProduto2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Compras.Produto.DatabaseProduto produto = new Database.Compras.Produto.DatabaseProduto();
            List<Database.Model.tb_produto_compra> list = produto.consultar();

            Database.Model.tb_produto_compra p = cboProduto2.SelectedItem as Database.Model.tb_produto_compra;
            int id = p.id_produto_compra;

            txtnome2.Text = p.nm_produto;
            nudpreco2.Value = p.vl_preco;
        }
    }
}
