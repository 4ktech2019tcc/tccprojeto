﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Compras
{
    public partial class frmCadastrarPedido : UserControl
    {
        public frmCadastrarPedido()
        {
            InitializeComponent();
        }

        private void CarregarComboFornecedor()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_fornecedor> list = db.tb_fornecedor.ToList();

            //list.Insert(0, new Database.Model.tb_fornecedor { nm_nome = "" });

            // carrega as informações no combo 
            cboFornecedor.DisplayMember = nameof(Database.Model.tb_fornecedor.nm_nome);
            cboFornecedor.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }
        private void CarregarComboPedido()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.ToList();

            //list.Insert(0, new Database.Model.tb_produto_compra { nm_produto = "" });

            // carrega as informações no combo 
            cboProduto.DisplayMember = nameof(Database.Model.tb_produto_compra.nm_produto);
            cboProduto.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime D = dtpDataDaCompra.Value;

                if (D < DateTime.Now)
                {
                    MessageBox.Show("Insira uma data valida", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
                else
                {
                    Database.Model.tb_compra compra = new Database.Model.tb_compra();

                    Database.Model.tb_fornecedor idFornecedor = cboFornecedor.SelectedItem as Database.Model.tb_fornecedor;
                    int id = idFornecedor.id_fornecedor;
                    
                    //insert tb compra
                    compra.fk_fornecedor = id;
                    compra.dt_compra = dtpDataDaCompra.Value.Date;

                    Business.Compras.Pedido.BusinessPedido busi = new Business.Compras.Pedido.BusinessPedido();
                    busi.CadastrarProduto(compra);
                    //

                    int idcompra = compra.id_compra;

                    List<Database.Model.tb_produto_compra> itens = dgvproduto.DataSource as List<Database.Model.tb_produto_compra>;
                    foreach (Database.Model.tb_produto_compra item in itens)
                    {
                        Database.Model.tb_compra_item compra_item = new Database.Model.tb_compra_item();

                        compra_item.fk_id_produto_compra = item.id_produto_compra;
                        compra_item.fk_id_compra = idcompra;

                        busi.CadastrarCompraitem(compra_item);

                        //inser no estoque

                        Database.Model.tb_estoque esto = new Database.Model.tb_estoque();
                        esto.fk_poduto_compra = compra_item.fk_id_produto_compra;
                        esto.qt_estoque = 1;

                        Database.Compras.Estoque.DatabaseEstoque estoque = new Database.Compras.Estoque.DatabaseEstoque();
                        estoque.Cadastrar(esto);
                    }

                    ////inser no estoque
                    //Database.Model.tb_estoque esto = new Database.Model.tb_estoque();
                    //esto.fk_poduto_compra = ;
                    //esto.qt_estoque = Convert.ToInt32(nudquantidade.Value);

                    //Database.Compras.Estoque.DatabaseEstoque estoque = new Database.Compras.Estoque.DatabaseEstoque();
                    //estoque.Cadastrar(esto);

                    MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    dgvproduto.DataSource = null;

                }

                // carrega modelo compra
                // salva compra

                // foreach nos itens da grid
                // salvar na compra_item

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                int qtd = Convert.ToInt32(nudquantidade.Value);
                if (qtd == 0)
                {
                    MessageBox.Show("Insira a Quantidade", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
                else
                {
                    Database.Model.tb_produto_compra p = cboProduto.SelectedItem as Database.Model.tb_produto_compra;

                    List<Database.Model.tb_produto_compra> itens = dgvproduto.DataSource as List<Database.Model.tb_produto_compra>;

                    if (itens == null)
                        itens = new List<Database.Model.tb_produto_compra>();

                    for (int i = 0; i < qtd; i++)
                    {
                        itens.Add(p);
                    }

                    dgvproduto.DataSource = null;
                    dgvproduto.DataSource = itens;
                }

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        public void CboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Compras.Produto.DatabaseProduto produto = new Database.Compras.Produto.DatabaseProduto();
            List<Database.Model.tb_produto_compra> list = produto.consultar();

            Database.Model.tb_produto_compra P = cboProduto.SelectedItem as Database.Model.tb_produto_compra;

            decimal valor = P.vl_preco;
            lblUnidade.Text = Convert.ToString(valor);

            decimal quantidade = Convert.ToInt32(nudquantidade.Value);

            decimal ValorTotal = quantidade * valor;

            lblTotal.Text = Convert.ToString(ValorTotal);

        }
        private void CboProduto_Click(object sender, EventArgs e)
        {
            this.CarregarComboPedido();
        }
        private void CboFornecedor_Click(object sender, EventArgs e)
        {
            this.CarregarComboFornecedor();
        }
        private void nudquantidade_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.CboProduto_SelectedIndexChanged(sender, e);
            }
            catch (Exception)
            {
                MessageBox.Show("Insira um produto antes", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }     
        }
    }
}
