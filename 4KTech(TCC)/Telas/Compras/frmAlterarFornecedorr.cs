﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Compras
{
    public partial class frmAlterarFornecedorr : UserControl
    {
        public frmAlterarFornecedorr()
        {
            InitializeComponent();
        }

        private void CarregarCombo()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_fornecedor> list = db.tb_fornecedor.ToList();

            list.Insert(0, new Database.Model.tb_fornecedor { nm_nome = "" });

            // carrega as informações no combo 
            cbofornecedor.DisplayMember = nameof(Database.Model.tb_fornecedor.nm_nome);
            cbofornecedor.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_fornecedor fornecedor1 = cbofornecedor.SelectedItem as Database.Model.tb_fornecedor;
                int id = fornecedor1.id_fornecedor;
                Database.Model.tb_fornecedor fornecedor = new Database.Model.tb_fornecedor();

                fornecedor.id_fornecedor = id;
                fornecedor.nm_nome = txtNome.Text;
                fornecedor.ds_CEP = txtCEP.Text;
                fornecedor.ds_estado = cboEstado.Text;
                fornecedor.ds_telefone = txtTel.Text;
                fornecedor.ds_CNPJ = txtCNPJ.Text;
                fornecedor.ds_cidade = txtCidade.Text;
                fornecedor.ds_rua = txtRua.Text;
                fornecedor.ds_bairro = txtBairro.Text;
                fornecedor.ds_email = txtemail.Text;

                Business.Compras.BusinessFornecedor f = new Business.Compras.BusinessFornecedor();
                f.Alterarfornecedor(fornecedor);

                MessageBox.Show("Alterado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void Cbofornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Model.tb_fornecedor fornecedor = cbofornecedor.SelectedItem as Database.Model.tb_fornecedor;
            string nome = fornecedor.nm_nome;
            Database.funcionario.DatabasefuncionarioConsulta f = new Database.funcionario.DatabasefuncionarioConsulta();
            List<Database.Model.tb_fornecedor> list = f.consulF(nome);

            txtNome.Text = fornecedor.nm_nome;
            txtCEP.Text = fornecedor.ds_CEP;
            cboEstado.Text = fornecedor.ds_estado;
            txtTel.Text = fornecedor.ds_telefone;
            txtCNPJ.Text = fornecedor.ds_CNPJ;
            txtCidade.Text = fornecedor.ds_cidade;
            txtRua.Text = fornecedor.ds_rua;
            txtBairro.Text = fornecedor.ds_bairro;
            txtemail.Text = fornecedor.ds_email;
        }

        private void Cbofornecedor_Click(object sender, EventArgs e)
        {
            this.CarregarCombo();
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_fornecedor fornecedor1 = cbofornecedor.SelectedItem as Database.Model.tb_fornecedor;
                int id = fornecedor1.id_fornecedor;

                if (id == 0)
                {
                    MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    return;
                }

                Database.Compras.Fornedor.DatabaseFornecedor db = new Database.Compras.Fornedor.DatabaseFornecedor();
                db.Remover(id);

                MessageBox.Show("fornecedor removido", "sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (Exception)
            {

                MessageBox.Show("erro", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
