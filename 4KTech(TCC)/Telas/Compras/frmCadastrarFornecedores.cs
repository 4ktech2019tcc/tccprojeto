﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Compras
{
    public partial class frmCadastrarFornecedores : UserControl
    {
        public frmCadastrarFornecedores()
        {
            InitializeComponent();
        }

        public void loadCEP()
        {
            try
            {
                string cep = txtcep.Text;

                Diferenciais.Correio.CorreioApi correiorua = new Diferenciais.Correio.CorreioApi();
                string rua = correiorua.BuscarRUa(cep);

                Diferenciais.Correio.CorreioApi correiobairro = new Diferenciais.Correio.CorreioApi();
                string bairro = correiobairro.BuscarBairro(cep);

                // eh cidade 
                Diferenciais.Correio.CorreioApi correioestado = new Diferenciais.Correio.CorreioApi();
                string estado = correioestado.BuscarEstado(cep);

                txtRua.Text = rua;

                txtCidade.Text = estado;
                txtBairro.Text = bairro;
            }
            catch (Exception)
            {

                MessageBox.Show("complete os campos");
            }
        }



        private void Button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                // pega o email do cliente
                string email = txtemail.Text.Trim();

                // valida o email
                Database.valid.ValidarEmail email1 = new Database.valid.ValidarEmail();
                int valição = email1.email(email);

                if (valição == 1) 
                {
                    
                    Database.Model.tb_fornecedor fornecedor = new Database.Model.tb_fornecedor();

                    fornecedor.nm_nome = txtNome.Text.Trim();
                    fornecedor.ds_CEP = txtcep.Text;
                    fornecedor.ds_estado = cboEstado.Text;
                    fornecedor.ds_telefone = txtTel.Text.Trim();
                    fornecedor.ds_CNPJ = txtCNPJ.Text.Trim();
                    fornecedor.ds_cidade = txtCidade.Text;
                    fornecedor.ds_rua = txtRua.Text;
                    fornecedor.ds_bairro = txtBairro.Text;
                    fornecedor.ds_email = txtemail.Text;


                    Business.Compras.BusinessFornecedor f = new Business.Compras.BusinessFornecedor();
                    f.cadastrarfornecedor(fornecedor);

                    MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    txtNome.Text = null;
                    txtcep.Text = null;
                    cboEstado.Text = null;
                    txtTel.Text = null;
                    txtCNPJ.Text = null;
                    txtCidade.Text = null;
                    txtRua.Text = null;
                    txtBairro.Text = null;
                    txtemail.Text = null;

                }
                else
                {
                    MessageBox.Show("email invalido", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
             
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
           
        }

        private void Button3_Click(object sender, EventArgs e)
        {

            string nome = txtFornecedorCadas.Text.Trim();

           if (txtFornecedorCadas.Text == string.Empty)
            {
                MessageBox.Show("erro");
                return;
            }


            Database.Compras.Fornedor.DatabaseFornecedor db = new Database.Compras.Fornedor.DatabaseFornecedor();
            List<Database.Model.tb_fornecedor> list = db.consultar(nome);

            dgvfornecedores.DataSource = list;

            



        }

        private void Txtcep_Leave(object sender, EventArgs e)
        {
            this.loadCEP();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
                List<Database.Model.tb_fornecedor> list = db.tb_fornecedor.ToList();

                dgvfornecedores.DataSource = list;
            }
            catch (Exception)
            {

                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }
    }
}
