﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Compras
{
    public partial class dtpdata : Form
    {
        public dtpdata()
        {
            InitializeComponent();
            this.CarregarComboFornecedor();
            this.CarregarComboPedido();
        }


        private void CarregarComboFornecedor()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_fornecedor> list = db.tb_fornecedor.ToList();

            list.Insert(0, new Database.Model.tb_fornecedor { nm_nome = "" });

            // carrega as informações no combo 
            cboFornecedor.DisplayMember = nameof(Database.Model.tb_fornecedor.nm_nome);
            cboFornecedor.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }
        private void CarregarComboPedido()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Database.Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.ToList();

            list.Insert(0, new Database.Model.tb_produto_compra { nm_produto = "" });

            // carrega as informações no combo 
            cboProduto.DisplayMember = nameof(Database.Model.tb_produto_compra.nm_produto);
            cboProduto.DataSource = list;

            //cbolistfuncionario.Items.Insert(0, "Selecione");

        }


        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Model.tb_produto_compra p = cboProduto.SelectedItem as Database.Model.tb_produto_compra;

                List<Database.Model.tb_produto_compra> itens = dgtProduto.DataSource as List<Database.Model.tb_produto_compra>;

                if (itens == null)
                    itens = new List<Database.Model.tb_produto_compra>();

                itens.Add(p);

                dgtProduto.DataSource = null;
                dgtProduto.DataSource = itens;

                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnSalvar_Click(object sender, EventArgs e) 
        {
            try
            {
                Database.Model.tb_compra compra = new Database.Model.tb_compra();
                Database.Model.tb_compra_item compra_item = new Database.Model.tb_compra_item();

                Database.Model.tb_fornecedor idFornecedor = cboFornecedor.SelectedItem as Database.Model.tb_fornecedor;
                int id = idFornecedor.id_fornecedor;

                compra.fk_fornecedor = id;
                compra.dt_compra = dtpDataDaCompra.Value.Date;

                Business.Compras.Pedido.BusinessPedido busi = new Business.Compras.Pedido.BusinessPedido();
                busi.CadastrarProduto(compra);

                Database.Compras.Pedido.DatabasePedido C = new Database.Compras.Pedido.DatabasePedido();
                int idcompra = C.aultimoPidido(compra);

                Database.Model.tb_produto_compra idp = cboProduto.SelectedItem as Database.Model.tb_produto_compra;
                int id2 = idp.id_produto_compra;

                compra_item.fk_id_produto_compra = id2;
                compra_item.fk_id_compra = idcompra;

                busi.CadastrarCompraitem(compra_item);


                //inser no estoque
                Database.Model.tb_estoque esto = new Database.Model.tb_estoque();
                esto.fk_poduto_compra = id2;
                esto.qt_estoque = Convert.ToInt32 (nudquantidade.Value);

                Database.Compras.Estoque.DatabaseEstoque estoque = new Database.Compras.Estoque.DatabaseEstoque();
                estoque.Cadastrar(esto);
                


                MessageBox.Show("Cadastrado com sucesso", "Chocobunny", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                // carreg modelo compra
                // salva compra

                // foreach nos itens da grid
                // salvar na compra_item
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro", "Chocobunny", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void dgtProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Compras.Produto.DatabaseProduto produto = new Database.Compras.Produto.DatabaseProduto();
            List<Database.Model.tb_produto_compra> list = produto.consultar();

            Database.Model.tb_produto_compra P = cboProduto.SelectedItem as Database.Model.tb_produto_compra;

            decimal valor = P.vl_preco;
            lblUnidade.Text = Convert.ToString(valor);

            decimal quantidade = Convert.ToInt32(nudquantidade.Value);

            decimal ValorTotal = quantidade * valor;

            lblTotal.Text = Convert.ToString( ValorTotal);


        }

        private void nudquantidade_ValueChanged(object sender, EventArgs e)
        {
            //int valor = Convert.ToInt32( lblUnidade.Text);
            //decimal quantidade = Convert.ToInt32(nudquantidade.Value);

            //decimal ValorTotal = quantidade * valor;

            //lblTotal.Text = Convert.ToString(ValorTotal);
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
             
        }
    }
}
