﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_.Telas.Logistica
{
    public partial class FrmConsutarEstoque : UserControl
    {
        public FrmConsutarEstoque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNome.Text.Trim();
                if (nome == string.Empty)
                {
                    MessageBox.Show("erro", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Database.Compras.Estoque.DatabaseEstoque db = new Database.Compras.Estoque.DatabaseEstoque();
                List<Database.Model.vw_consular_estoque_top> list = db.Consultar(nome);

                dgtConsulta.DataSource = list;
            }
            catch (Exception)
            {

                MessageBox.Show("Procure um funcionario da 4K tech", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DgtConsulta_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TxtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {


            try
            {
                Database.Compras.Estoque.DatabaseEstoque db = new Database.Compras.Estoque.DatabaseEstoque();
                List<Database.Model.vw_consular_estoque_top> list = db.ConsultarPorTudo();

                dgtConsulta.DataSource = list;
            }
            catch (Exception)
            {
                MessageBox.Show("Procure um funcionario da 4K tech", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
