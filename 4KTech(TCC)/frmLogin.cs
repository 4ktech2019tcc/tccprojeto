﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4KTech_TCC_
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            { 

                // pega os parâmetros
                string user = txtuser.Text;
                string senha = txtsenha.Text.Trim();
                
                string chave = "1234567890123456";


                Diferenciais.Criptografia.AEScrip crip = new Diferenciais.Criptografia.AEScrip();

                string SenhaCriptografada = crip.Criptografar(chave, senha);



                Business.Logar_usuario.BusinessLoginUser busi = new Business.Logar_usuario.BusinessLoginUser();
                Database.Model.tb_funcionario validação = busi.Logar(user, SenhaCriptografada);
                     

                // faz uma validação se retornar null o usuario não e logado

                if (validação != null)
                {
                    Database.Validação.UsuarioLogado.nomeuserLogado = user;

                                       

                    Database.Model.tb_funcionario funcionario = new Database.Model.tb_funcionario();
                    Database.funcionario.DatabasefuncionarioConsulta f = new Database.funcionario.DatabasefuncionarioConsulta();
                    Database.Model.tb_funcionario informaçoes = f.consulValidacao(user);

                    Database.Logar_usuario.UsuarioLogado.IDUsuario = informaçoes.id_funcionario;
                    Database.Logar_usuario.UsuarioLogado.NomeUsuario = informaçoes.nm_funcionario;
                    

                    Database.Validação.UsuarioLogado.IDuserLogado = funcionario.id_funcionario;

                    Telas.FrmMenu tela = new Telas.FrmMenu();
                    tela.Show();
                    this.Hide();

                    //Telas.TesteAsTelasAqui t = new Telas.TesteAsTelasAqui();
                    //t.Show();
                    //this.Hide();
                }
                else
                {
                    MessageBox.Show("usuario ou senha invalidos");
                    return;

                }
            }
            catch (Exception)
            {

                MessageBox.Show("erro tente mais tarde", "erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult resposta = MessageBox.Show("Deseja realmente sair?", "4k tech",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

            if (resposta == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void fileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {

        }

        private void Button1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            Button1_Click(sender, e);
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void Button1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                Button1_Click(sender, e);
        }

        private void Button1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Button1_Click(sender, e);
        }

        private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Button1_Click(sender, e);
        }

        private void FrmLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                Button1_Click(sender, e);
        }

        private void FrmLogin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Button1_Click(sender, e);
        }

        private void Txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                Button1_Click(sender, e);
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            // abre o google em uma pagina
            System.Diagnostics.Process.Start("chrome.exe", "https://chocobunny2019tcc12.pancakeapps.com/");
        }
    }
}
