﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Text.RegularExpressions;
using System.Net.Configuration;

namespace _4KTech_TCC_.Diferenciais.E_mail
{
    class EmailAPI
    {
        public void Enviar(string paraQuem)
        {
            Task.Factory.StartNew(() =>
            {
                // Se for usar seu email, habilite ele em: https://myaccount.google.com/lesssecureapps?pli=1
                string remetente = "4ktech2019tcc@gmail.com";
                string senha = "4ktech2019tcc1234";

                string m = "Olá essa e um email de confirmação de cadastro não o responda";

                string assunto = "Chocobunny Café";
                

                // Configura a mensagem
                MailMessage email = new MailMessage();

                // Configura Remetente, Destinatário
                email.From = new MailAddress(remetente);
                email.To.Add(paraQuem);

                // Configura Assunto, Corpo e se o Corpo está em Html
                email.Subject = assunto;
                email.Body = m;
                email.IsBodyHtml = true;
                              
                // Configura os parâmetros do objeto SMTP
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;


                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(remetente, senha);

                // Envia a mensagem
                smtp.Send(email);

            });
        }

    }
}
