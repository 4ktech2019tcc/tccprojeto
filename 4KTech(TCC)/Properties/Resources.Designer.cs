﻿//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _4KTech_TCC_.Properties {
    using System;
    
    
    /// <summary>
    ///   Uma classe de recurso de tipo de alta segurança, para pesquisar cadeias de caracteres localizadas etc.
    /// </summary>
    // Essa classe foi gerada automaticamente pela classe StronglyTypedResourceBuilder
    // através de uma ferramenta como ResGen ou Visual Studio.
    // Para adicionar ou remover um associado, edite o arquivo .ResX e execute ResGen novamente
    // com a opção /str, ou recrie o projeto do VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Retorna a instância de ResourceManager armazenada em cache usada por essa classe.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("_4KTech_TCC_.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Substitui a propriedade CurrentUICulture do thread atual para todas as
        ///   pesquisas de recursos que usam essa classe de recurso de tipo de alta segurança.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _203908_como_encontrar_um_fornecedor_de_qualidade_para_a_sua_empresa {
            get {
                object obj = ResourceManager.GetObject("203908-como-encontrar-um-fornecedor-de-qualidade-para-a-sua-empresa", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap _4k_tech_logo_solo_branco {
            get {
                object obj = ResourceManager.GetObject("4k tech logo solo branco", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap bonds_20px {
            get {
                object obj = ResourceManager.GetObject("bonds_20px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap bunny_jump {
            get {
                object obj = ResourceManager.GetObject("bunny_jump", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap cadastro {
            get {
                object obj = ResourceManager.GetObject("cadastro", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap cadastro_white {
            get {
                object obj = ResourceManager.GetObject("cadastro white", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap cancel_120_azul_px {
            get {
                object obj = ResourceManager.GetObject("cancel_120_azul_px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap cancel_120px {
            get {
                object obj = ResourceManager.GetObject("cancel_120px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap capa_fluxo_de_caixa {
            get {
                object obj = ResourceManager.GetObject("capa-fluxo-de-caixa", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap cardboard_box_20px {
            get {
                object obj = ResourceManager.GetObject("cardboard_box_20px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ChocoBunny {
            get {
                object obj = ResourceManager.GetObject("ChocoBunny", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ChocoBunnyPNG_Logo {
            get {
                object obj = ResourceManager.GetObject("ChocoBunnyPNG-Logo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap chrome_120px {
            get {
                object obj = ResourceManager.GetObject("chrome_120px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap chrome_color_120px {
            get {
                object obj = ResourceManager.GetObject("chrome_color_120px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap chrome_color_26px {
            get {
                object obj = ResourceManager.GetObject("chrome_color_26px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap depositphotos_113857140_stock_illustration_shop_product_basket_vector_illustration {
            get {
                object obj = ResourceManager.GetObject("depositphotos_113857140-stock-illustration-shop-product-basket-vector-illustratio" +
                        "n", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap fork_lift_20px {
            get {
                object obj = ResourceManager.GetObject("fork_lift_20px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap fundo_lista_de_produtos_1300_243 {
            get {
                object obj = ResourceManager.GetObject("fundo-lista-de-produtos_1300-243", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap gif_4ktech_em_pix_rosa {
            get {
                object obj = ResourceManager.GetObject("gif-4ktech-em-pix_rosa", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap giphy {
            get {
                object obj = ResourceManager.GetObject("giphy", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap home_120px {
            get {
                object obj = ResourceManager.GetObject("home_120px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap inventario {
            get {
                object obj = ResourceManager.GetObject("inventario", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap menu_120px {
            get {
                object obj = ResourceManager.GetObject("menu_120px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap minimize_window_120px {
            get {
                object obj = ResourceManager.GetObject("minimize_window_120px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap parse_from_clipboard_20px {
            get {
                object obj = ResourceManager.GetObject("parse_from_clipboard_20px", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap postrh_blog {
            get {
                object obj = ResourceManager.GetObject("postrh_blog", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap shopping_online_background_1156_96 {
            get {
                object obj = ResourceManager.GetObject("shopping-online-background_1156-96", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap venda_adicional_varejo {
            get {
                object obj = ResourceManager.GetObject("venda-adicional-varejo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Consulta um recurso localizado do tipo System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap vetores_exclusivos_de_atendimento_ao_cliente_7139_125 {
            get {
                object obj = ResourceManager.GetObject("vetores-exclusivos-de-atendimento-ao-cliente_7139-125", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
