﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Vendas.Produto
{
    class DatabaseProduto
    {
        public void salvar (Database.Model.tb_produto_venda v)
        {

            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_produto_venda.Add(v);
            db.SaveChanges();
        }

        public List<Model.tb_produto_venda> consultarNome (string nome)
        {

            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Model.tb_produto_venda> list = db.tb_produto_venda.Where(v => v.nm_produto.Contains(nome)).ToList();
            return list;
        }

        public void remover (int id)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_produto_venda remover = db.tb_produto_venda.First(v => v.id_produto_venda == id);
            db.tb_produto_venda.Remove(remover);
            db.SaveChanges();
        }
        public List<Model.tb_produto_venda> consultar()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Model.tb_produto_venda> list = db.tb_produto_venda.ToList();
            return list;

        }

        public void alterar(Model.tb_produto_venda f)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_produto_venda altear = db.tb_produto_venda.First(t => t.id_produto_venda == f.id_produto_venda);

            altear.nm_produto = f.nm_produto;
            altear.vl_preço = f.vl_preço;

            db.SaveChanges();

        }

    }
}






            






