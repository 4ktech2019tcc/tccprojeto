﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Vendas.Cliente
{
    class DatabaseCliente
    {
        public void cadastrar(Database.Model.tb_cliente c)
        {

            Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_cliente.Add(c);
            db.SaveChanges();


        }

        public void alterar(Database.Model.tb_cliente c)

        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_cliente alterar = db.tb_cliente.First(t => t.id_cliente == c.id_cliente);



            alterar.nm_nome = c.nm_nome;
            alterar.ds_bairro = c.ds_bairro;
            alterar.ds_celular = c.ds_celular;
            alterar.ds_cidade = c.ds_cidade;
            alterar.ds_cliente = c.ds_cliente;
            alterar.ds_CPF = c.ds_CPF;
            alterar.ds_email = c.ds_email;
            alterar.ds_estado = c.ds_estado;
            alterar.ds_rua = c.ds_rua;
            alterar.ds_telefone = c.ds_telefone;
            alterar.ds_CEP = c.ds_CEP;

            db.SaveChanges();

        }


        public List<Database.Model.tb_cliente> consultar(string nome)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_cliente> list = db.tb_cliente.Where(c => c.nm_nome == nome).ToList();
            return list;


        }

        public void  remover (int id)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_cliente remover = db.tb_cliente.First(t => t.id_cliente == id);
            db.tb_cliente.Remove(remover);
            db.SaveChanges();

        }

    }
}





       













