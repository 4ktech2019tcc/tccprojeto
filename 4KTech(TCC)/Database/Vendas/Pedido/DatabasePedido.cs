﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Vendas.Pedido
{
    class DatabasePedido
    {
        public void Cadastrarvanda(Model.tb_venda venda)
        {
            Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_venda.Add(venda);
            db.SaveChanges();
        }
        public int aultimoPidido(Model.tb_venda A)
        {
            Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Model.tb_venda> venda = db.tb_venda.ToList();
            int ultimaChamada = venda.Max(t => t.id_venda);

            return ultimaChamada;
        }
        public void Cadastrarvendaitem(Model.tb_venda_item A)
        {
            Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_venda_item.Add(A);
            db.SaveChanges();
        }
    }
}

