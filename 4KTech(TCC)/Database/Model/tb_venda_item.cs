//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _4KTech_TCC_.Database.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_venda_item
    {
        public int id_venda_item { get; set; }
        public int fk_id_venda { get; set; }
        public int fk_id_produto_venda { get; set; }
    
        public virtual tb_produto_venda tb_produto_venda { get; set; }
        public virtual tb_venda tb_venda { get; set; }
    }
}
