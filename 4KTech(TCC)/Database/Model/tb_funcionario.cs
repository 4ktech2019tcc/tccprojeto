//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _4KTech_TCC_.Database.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_funcionario
    {
        public tb_funcionario()
        {
            this.tb_folha_pagamento = new HashSet<tb_folha_pagamento>();
            this.tb_ponto = new HashSet<tb_ponto>();
        }
    
        public int id_funcionario { get; set; }
        public string nm_funcionario { get; set; }
        public string ds_endereco { get; set; }
        public string ds_RG { get; set; }
        public string ds_CPF { get; set; }
        public string ds_usuario { get; set; }
        public string ds_senha { get; set; }
        public decimal vl_salario { get; set; }
        public Nullable<decimal> vl_vt { get; set; }
        public Nullable<decimal> vl_va { get; set; }
        public Nullable<decimal> vl_vr { get; set; }
        public Nullable<decimal> vl_convenio { get; set; }
        public Nullable<bool> bt_admin { get; set; }
        public Nullable<bool> bt_funcionario { get; set; }
        public Nullable<bool> bt_permissao_RH { get; set; }
        public Nullable<bool> bt_permissao_vendas { get; set; }
        public Nullable<bool> bt_permissao_compras { get; set; }
        public Nullable<bool> bt_permissao_financeiro { get; set; }
        public Nullable<bool> bt_permissao_logistica { get; set; }
        public string ds_celular { get; set; }
    
        public virtual ICollection<tb_folha_pagamento> tb_folha_pagamento { get; set; }
        public virtual ICollection<tb_ponto> tb_ponto { get; set; }
    }
}
