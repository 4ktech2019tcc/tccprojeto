﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Compras.Produto
{
    class DatabaseProduto
    {
        public void CadastrarProduto(Model.tb_produto_compra A)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_produto_compra.Add(A);
            db.SaveChanges();
        }
        public void AlterarProduto(Model.tb_produto_compra A)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_produto_compra altear = db.tb_produto_compra.First(t => t.id_produto_compra == A.id_produto_compra);

            altear.nm_produto = A.nm_produto;
         
            altear.vl_preco = A.vl_preco;
           

            db.SaveChanges();
        }
        public List<Model.tb_produto_compra> consultar()

        {

            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Model.tb_produto_compra> list = db.tb_produto_compra.ToList();

            return list;


        }
    }
}
