﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Compras.Fornedor
{
    class DatabaseFornecedor
    {
        public void CadastrarFornecedor (Model.tb_fornecedor f)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_fornecedor.Add(f);
            db.SaveChanges();
        }

        public void Remover(int id)
        {
            
                Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
                Model.tb_fornecedor remover = db.tb_fornecedor.First(t => t.id_fornecedor == id);
                db.tb_fornecedor.Remove(remover);

                db.SaveChanges();
       
        }
        public void AlterarFornecedor(Model.tb_fornecedor f)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_fornecedor alterar = db.tb_fornecedor.First(t => t.id_fornecedor == f.id_fornecedor);

            

            alterar.nm_nome = f.nm_nome;
            alterar.ds_bairro = f.ds_bairro;
            alterar.ds_CEP = f.ds_CEP;
            alterar.ds_cidade = f.ds_cidade;
            alterar.ds_CNPJ = f.ds_CNPJ;
            alterar.ds_email = f.ds_email;
            alterar.ds_estado = f.ds_estado;
            alterar.ds_rua = f.ds_rua;
            alterar.ds_telefone = f.ds_telefone;
            


            db.SaveChanges();
        }
        public List<Model.tb_fornecedor> consultar (string nome)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Model.tb_fornecedor> list = db.tb_fornecedor.Where(f => f.nm_nome.Contains(nome)).ToList();
            return list;
        }

        public List<Model.tb_fornecedor> consultarPorID(int ID)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Model.tb_fornecedor> list = db.tb_fornecedor.Where(f => f.id_fornecedor == ID).ToList();
            return list;
        }



    }
}
