﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Compras.Estoque
{
    class DatabaseEstoque
    {
        public void Cadastrar(Model.tb_estoque estoque)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_estoque.Add(estoque);
            db.SaveChanges();
        }

        public List<Model.vw_consular_estoque_top> Consultar(string nome)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.vw_consular_estoque_top> list = db.vw_consular_estoque_top.Where(t => t.nm_produto.Contains(nome)).ToList();
            return list;
        }

        public List<Model.vw_consular_estoque_top> ConsultarPorTudo()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.vw_consular_estoque_top> list = db.vw_consular_estoque_top.ToList();
            return list;
        }

    }
}
