﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.Financeiro.FluxoDeCaixa
{
    class DatabaseFluxoDeCaixa
    {

        public List<Database.Model.vw_consultar_fluxodecaixa> consultar(DateTime data)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
           DateTime mes =Convert.ToDateTime (data.Month);
            List<Database.Model.vw_consultar_fluxodecaixa> list = db.vw_consultar_fluxodecaixa.Where(c => c.dt_referencia ==data).ToList();
            return list;


        }

        public List<Database.Model.vw_consultar_fluxodecaixa> consultarPortudo()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.vw_consultar_fluxodecaixa> list = db.vw_consultar_fluxodecaixa.ToList();
            return list;


        }
    }
}
