﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.funcionario
{
    class DatabasefuncionarioConsulta
    {

        public List<Database.Model.tb_funcionario> consul(string nome)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_funcionario> list = db.tb_funcionario.Where(f=> f.nm_funcionario == nome).ToList();
            return list;


        }
        public Database.Model.tb_funcionario consulValidacao(string User)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Database.Model.tb_funcionario nome = db.tb_funcionario.First(f => f.ds_usuario == User);
            return nome;


        }
        public List<Database.Model.tb_fornecedor> consulF(string nome)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_fornecedor> list = db.tb_fornecedor.Where(f => f.nm_nome == nome).ToList();
            return list;


        }
        public List<Database.Model.tb_produto_compra> consulPro(string nome)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_produto_compra> list = db.tb_produto_compra.Where(f => f.nm_produto == nome).ToList();
            return list;


        }
    }
}
