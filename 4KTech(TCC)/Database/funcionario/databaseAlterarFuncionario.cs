﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.funcionario
{
    class databaseAlterarFuncionario
    {

        public void alterar(Model.tb_funcionario f)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Model.tb_funcionario altear = db.tb_funcionario.First(t => t.id_funcionario == f.id_funcionario);


            altear.nm_funcionario = f.nm_funcionario;
            altear.ds_endereco = f.ds_endereco;
            altear.vl_salario = f.vl_salario;
            altear.vl_vt = f.vl_vt;
            altear.vl_va = f.vl_va;
            altear.vl_vr = f.vl_vr;
            altear.vl_convenio = f.vl_convenio;
            altear.ds_usuario = f.ds_usuario;
            altear.ds_senha = f.ds_senha;
            altear.bt_admin = f.bt_admin;
            altear.bt_funcionario = f.bt_funcionario;
            altear.bt_permissao_RH = f.bt_permissao_compras;
            altear.bt_permissao_compras = f.bt_permissao_compras;
            altear.bt_permissao_logistica = f.bt_permissao_logistica;
            altear.bt_permissao_vendas = f.bt_permissao_vendas;
            altear.bt_permissao_financeiro = f.bt_permissao_financeiro;
            altear.ds_CPF = f.ds_CPF;
            altear.ds_RG = f.ds_RG;

            db.SaveChanges();

        }
    }
}
