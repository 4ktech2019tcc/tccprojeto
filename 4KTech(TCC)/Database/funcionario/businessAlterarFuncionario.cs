﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.funcionario
{
    class businessAlterarFuncionario
    {
        public void alterar (Database.Model.tb_funcionario f)
        {
            if (f.nm_funcionario == string.Empty)
            {
                throw new ArgumentException("Nome do funcionario é obrigatório");

            }

            if (f.ds_endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");

            }

            if (f.vl_salario == 0)
            {
                throw new ArgumentException("Salário é obrigatório");

            }

            if (f.ds_usuario == string.Empty)
            {
                throw new ArgumentException("Nome do usuário é obrigatório");

            }

            if (f.ds_senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatório");

            }
            if (f.ds_celular == string.Empty)
            {
                throw new ArgumentException("informe o numero");

            }

            Database.funcionario.databaseAlterarFuncionario db = new databaseAlterarFuncionario();
            db.alterar(f);

        }

    }
}
