﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.RH.FolhaDePagamento
{
    class DatabaseFolhaDePagamento
    {
        public void CadastrarFolha(Model.tb_folha_pagamento f)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_folha_pagamento.Add(f);
            db.SaveChanges();


        }
        public List<Model.vw_consultar_folha> Consultar(string nome )
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.vw_consultar_folha> list = db.vw_consultar_folha.Where(t => t.nm_funcionario.Contains(nome)).ToList();
            return list;
        }

        public List<Model.vw_consultar_folha> ConsultarTODO()
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.vw_consultar_folha> list = db.vw_consultar_folha.ToList();
            return list;
        }
    }
}
