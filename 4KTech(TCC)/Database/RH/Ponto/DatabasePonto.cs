﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4KTech_TCC_.Database.RH.Ponto
{
    class DatabasePonto
    {
        public void Cadastrar(Model.tb_ponto ponto)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            db.tb_ponto.Add(ponto);
            db.SaveChanges();
        }
        public List<Database.Model.tb_ponto> Consutar(int ID)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            List<Database.Model.tb_ponto> list = db.tb_ponto.Where(p => p.id_Funcionario == ID).ToList();
            return list;
        }
        public void Remover(int ID)
        {
            Database.Model.db_a4e8a0_chocoEntities db = new Model.db_a4e8a0_chocoEntities();
            Database.Model.tb_ponto remover = db.tb_ponto.First(p => p.id_Ponto == ID);
            db.tb_ponto.Remove(remover);
            db.SaveChanges();
        }


    }
}
